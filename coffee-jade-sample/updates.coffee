
Template.updates.helpers
  updates: ->
    OrderUpdates.find {}, sort: {createdAt: -1}

Template.updates.events
  'click .btn.delete': (event, tmpl) ->
    if confirm 'are you sure'
      OrderUpdates.remove @_id
