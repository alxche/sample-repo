
Template.updateEdit.helpers
  defaultOptions: ->
    Meteor.users.find _id: $in: @update.users or []
      .map (user) ->
        label: user.displayName(), value: user._id

Template.updateEdit.events
