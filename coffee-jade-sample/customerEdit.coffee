
Template.customerEdit.onCreated (tmpl = @) ->

Template.customerEdit.helpers

  order: ->
    Orders.findOne(@_id)

  schema: ->
    new SimpleSchema
      email:
        type: String
        regEx: SimpleSchema.RegEx.Email
      shipping:
        type: Object
      'shipping.address':
        type: Object
        blackbox: true
      'shipping.name':
        type: String
      'shipping.phone':
        type: String
        optional: true


Template.customerEdit.events

Template.customerEdit.onRendered (tmpl = @) ->


AutoForm.addHooks 'updateCustomer',
  onSuccess: ->
    $('#modal').modal('hide')
    swal 'Good Job', 'Customer has been saved', 'success'
