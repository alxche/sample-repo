
Template.workflow.helpers

  emails: ->
    if _.isArray(@emails) then @emails else []

Template.workflow.events

  'click .js-copy': (event, tmpl) ->
    workflow = _.pick(@, 'for', 'users', 'usersGroup', 'variants', 'title', 'emails')
    unless _.isArray(workflow.for)
      workflow.for = [workflow.for]
    Workflows.insert workflow,
      (err, workflowId) ->
        unless err
          app.success 'Workflow has been copied'
        else
          app.error err.message

  'click .js-publish': (event, tmpl) ->
    Workflows.update @_id,
      $set:
        isPublished: true
        publishedAt: new Date

  'click .js-unpublish': (event, tmpl) ->
    Workflows.update @_id,
      $set:
        isPublished: false
      $unset:
        publishedAt: 1
