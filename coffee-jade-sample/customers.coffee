
Template.customers.onCreated (tmpl = @) ->

  @selector = new ReactiveDict

  @options = new ReactiveDict
  @options.set
    limit: 100
    sort:
      purchasedAt: -1

  @autorun =>
    @subscribe 'orders', @selector.all(), @options.all()


Template.customers.helpers

  orders: ->
    tmpl = Template.instance()
    Orders.find tmpl.selector.all(), tmpl.options.all()

  hasMore: ->
    tmpl = Template.instance()
    Orders.find().count() >= tmpl.options.get('limit')

Template.customers.events

  'click .more': (event, tmpl) ->
    tmpl.options.set 'limit', tmpl.options.get('limit') + 50

  'click [data-toggle="offcanvas"]': (event, tmpl) ->
    TemplateVar.setTo '.customer-drawer', 'orderId', @_id

Template.customers.onRendered (tmpl = @) ->
  tmpl.$('input[name="daterange"]')
    .daterangepicker
      startDate: moment().startOf('month').toDate()
      showDropdowns: true
      locale:
        format: 'DD MMM YYYY'
    , (start, end) ->
      tmpl.selector.set 'purchasedAt',
        $lte: end.toDate()
        $gte: start.toDate()
    .on 'cancel.daterangepicker', (event, picker) ->
      tmpl.selector.delete 'purchasedAt'

AutoForm.addHooks 'searchCustomers',
  onSubmit: (doc) ->
    @event.preventDefault()
    tmpl = Template.instance().parent()
    if doc.productId
      tmpl.selector.set 'productId',
        $in: _.toArray(doc.productId)
    else
      tmpl.selector.delete 'productId'
    if doc.userId
      tmpl.selector.set 'userId',
        $in: _.toArray(doc.userId)
    else
      tmpl.selector.delete 'userId'
