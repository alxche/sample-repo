
Template.customersMenu.helpers

  showNewUpdateBtn: ->
    ActiveRoute.name 'updates'

  showNewWorkflowBtn: ->
    ActiveRoute.name 'workflows'

  showNewCustomerBtn: ->
    ActiveRoute.name 'customers'

Template.customersMenu.events

  'click [data-toggle="modal"]': (event, tmpl) ->
    TemplateVar.setTo '#modal', 'template', $(event.currentTarget).data('template')
    TemplateVar.setTo '#modal', 'data', @

  'click .new-workflow': (event, tmpl) ->
    Session.set 'newWorkflow', true