
Template.workflowAdd.helpers
  variants: ->
    product = Products.findOne AutoForm.getFieldValue('for')
    _.map product?.variants or [], (variant) ->
      name: "variants.v#{variant.categoryName}"
      label: variant.categoryName
      options: _.map variant.options, (option) ->
        label: option.name, value: option.name

Template.workflowAdd.events
  'click .cancel': (event, tmpl) ->
    Session.set 'newWorkflow', false