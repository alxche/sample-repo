Template.createCustomerModal.rendered = ->
  TemplateVar.set this, 'user', {}
Template.createCustomerModal.helpers

  user: ->
    TemplateVar.get 'user'

  product: ->
    TemplateVar.get 'product'

  price: ->
    product = TemplateVar.get('product')
    if product
      price = product.totalPrice
        variants: AutoForm.getFieldValue('variants')
        qty: AutoForm.getFieldValue('qty')
      parseFloat(price).toFixed(2)

  schema: ->
    product = TemplateVar.get('product')
    schemas = []
    if product
      schemas.push product.getPurchaseProductSchema()
    schemas.push Schemas.NewCustomer
    new SimpleSchema schemas

Template.createCustomerModal.events

  "autocompleteselect input[name='email']": (event, tmpl, doc) ->
    tmpl.$("[name='email']").val(doc.emails?[0].address)
    tmpl.$("[name='userId']").val(doc._id)
    tmpl.$("[name='shipping.name']").val(doc.profile.name)
    if !doc.profile.homeAddress and doc.profile.billingAddress
      doc.profile.homeAddress = doc.profile.billingAddress
    if doc.profile?.homeAddress
      address = doc.profile.homeAddress
      _.extend doc.profile.homeAddress,
        formattedAddress: "#{address.line}, #{address.city}, #{address.state} #{address.zip}, #{address.country}"
    TemplateVar.set tmpl, 'user', doc

  "autocompleteselect input[name='product']": (event, tmpl, doc) ->
    TemplateVar.set tmpl, 'product',
      Products.findOne doc._id
