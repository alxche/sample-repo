
Template.customerDrawer.onCreated ->
  TemplateVar.set @, 'orderId', null

Template.customerDrawer.helpers

  order: ->
    Orders.findOne TemplateVar.get('orderId')

  shipping: ->
    order = Orders.findOne(TemplateVar.get('orderId'))
    order.shipping or order.user().profile.billingAddress

Template.customerDrawer.events

  'click .js-edit': (event, tmpl) ->
    TemplateVar.setTo '#modal', 'template', 'customerEdit'
    TemplateVar.setTo '#modal', 'data', _id: TemplateVar.get(tmpl,'orderId')

  'click .js-refund': (event, tmpl) ->
    swal
      title: 'Do you want to refund order now?'
      text: 'You will not be able to recover them!'
      confirmButtonText: 'Yes, refund'
      type: 'warning'
      showCancelButton: true
      closeOnConfirm: false
      showLoaderOnConfirm: true
    , (isConfirm) =>
      if isConfirm
        Meteor.call "refundOrder", TemplateVar.get(tmpl,'orderId'), (err, refund) ->
          unless err
            swal 'You have been refund order', '', 'success'
          else
            console.log err
            swal err.message, '', 'error'

  'click .js-resend': (event, tmpl) ->
    swal
      title: 'Do you want to resend order email now?'
      text: ''
      confirmButtonText: 'Yes, resend'
      type: 'warning'
      showCancelButton: true
      closeOnConfirm: false
      showLoaderOnConfirm: true
    , (isConfirm) =>
      if isConfirm
        Meteor.call "sendPurchaseProductEmail", TemplateVar.get(tmpl,'orderId'), (err, result) ->
          unless err
            swal 'You have been resend email order successfully', '', 'success'
          else
            console.log err
            swal err.message, '', 'error'

  'click .js-remove': (event, tmpl) ->
    swal
      title: 'Do you want to remove order now?'
      text: ''
      confirmButtonText: 'Yes, delete'
      type: 'warning'
      showCancelButton: true
      closeOnConfirm: false
      showLoaderOnConfirm: true
    , (isConfirm) =>
      if isConfirm
        Meteor.call "removeOrder", TemplateVar.get(tmpl,'orderId'), (err, result) ->
          unless err
            swal '', 'You have been removed order successfully', 'success'
          else
            console.log err
            swal '', err.message, 'error'

  'click .js-reset-password': (event, tmpl) ->
    btn = $(event.currentTarget).button('loading')
    order = Orders.findOne TemplateVar.get(tmpl,'orderId')
    Accounts.forgotPassword email: order.emailAddress(), (err) ->
      btn.button('reset')
      unless err
        swal '', 'You have been reset password successfully', 'success'
      else
        swal '', err.message, 'error'


AutoForm.addHooks 'updateOrderShippingStatus',
  onSuccess: (type, orderId) ->
    swal '', 'You have been updated shipping status', 'success'
