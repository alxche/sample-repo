
Template.workflowEdit.helpers

  variants: ->
    product = null
    _.each AutoForm.getFieldValue('for'), (to) ->
      if Products.findOne(to)
        product = Products.findOne(to)
    _.map product?.variants or [], (variant) ->
      name: "variants.v#{variant.categoryName}"
      label: variant.categoryName
      options: _.map variant.options, (option) ->
        label: option.name, value: option.name

Template.workflowEdit.events

  'click .js-remove': (event, tmpl) ->
    swal
      title: 'Are you sure you want to delete workflow?'
      text: 'You will not be able to recover them!'
      type: 'warning'
      showCancelButton: true
      confirmButtonText: 'Yes, delete!'
      closeOnConfirm: false
    , =>
      Workflows.remove @workflow._id
      Router.go 'workflows'
      swal "Deleted!", "Your imaginary file has been deleted.", "success"

  'click .js-publish': (event, tmpl) ->
    modifier = $set: {}
    modifier.$set[@current.isPublished] = true
    modifier.$set[@current.publishedAt] = new Date
    Workflows.update Router.current().params._id, modifier

  'click .js-unpublish': (event, tmpl) ->
    modifier = $set: {}
    modifier.$set[@current.isPublished] = false
    modifier.$set[@current.publishedAt] = null
    Workflows.update Router.current().params._id, modifier

  'click .js-send-test': (event, tmpl) ->
    workflow = tmpl.data?.workflow
    if workflow
      email = workflow.emails[@index]
      Meteor.call 'sendTestWorkflowEmail', workflow._id, email.id, (err, result) ->
        unless err
          app.success 'Email has been sending'
        else
          app.error err.message
