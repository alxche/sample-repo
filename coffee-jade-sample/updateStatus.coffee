
Template.updateStatus.events

  'click .send': (event, tmpl) ->
    swal
      title: 'Do you want to send the email now?'
      #text: 'You will not be able to recover them!'
      confirmButtonText: 'Yes, send now'
      type: 'warning'
      showCancelButton: true
      closeOnConfirm: false
      showLoaderOnConfirm: true
    , (isConfirm) =>
      if isConfirm
        Meteor.call "sendUpdateEmail", @_id, (err, result) ->
          unless err
            swal 'Email sent to you', '', 'success'
          else
            swal err.message, '', 'error'

  'click .schedule': (event, tmpl) ->
    scheduleDate = moment(tmpl.$('[name="deliverAt"]').val(), 'MM/DD/YYYY h:mm A').toDate()
    if scheduleDate > new Date
      swal
        title: 'Do you want to schedule the email?'
        text: "Schedule Date <b>#{moment(scheduleDate).format('MMM D, YYYY h:mm a')}</b>"
        confirmButtonText: 'Yes, schedule now'
        type: 'warning'
        html: true
        showCancelButton: true
        closeOnConfirm: false
        showLoaderOnConfirm: true
      , (isConfirm) =>
        if isConfirm
          OrderUpdates.update @_id,
            $set:
              status: 'schedule'
              deliverAt: scheduleDate
          swal 'Email has been scheduled successfully', '', 'success'

  'click .cancel': (event, tmpl) ->
    swal
      title: 'Do you want cancel this schedule email?'
      text: ''
      confirmButtonText: 'Yes, cancel schedule'
      cancelButtonText: 'No, it is ok'
      type: 'warning'
      showCancelButton: true
      closeOnConfirm: false
      showLoaderOnConfirm: true
    , (isConfirm) =>
      if isConfirm
        OrderUpdates.update @_id,
          $set:
            status: 'pending'
          $unset:
            deliverAt: 1
        swal 'Email has been canceled successfully', '', 'success'


Template.updateStatus.onRendered ->
  $('.datetimepicker').datetimepicker()
