import React from 'react'

import ProductLessonView from './ProductLessonView.jsx'
import ProductCourseContent from './ProductCourseContent.jsx'

class ProductCourseViewComponent extends React.Component {

  constructor(props) {
    super(props)

    this.state = {
    }
  }

  renderProductNotFound() {
    return (
      <div className="row">
        <div className="col-md-8 col-md-offset-2 text-center margin-top-100">
          <div className="alert alert-warning m-b-10" >This Product doesn't exist or not published yet</div>
        </div>
      </div>
    )
  }

  renderProduct() {
    const { product, order, lessons } = this.props
    const author = product.author()
    const lesson = product.firstLesson()

    return (
      <div className="">
        <div className="row">
          <div className="col-lg-3">
            <ProductCourseContent product={product} lessons={lessons} lesson={lesson} />
          </div>
          <div className="col-lg-9">
            {/*<div className="content fr-view text-black" dangerouslySetInnerHTML={{ __html: product.description }} />*/}
            <ProductLessonView lesson={lesson} order={order} />
          </div>
        </div>
        {/*<div className="footer p-tb-20 p-lr-10 text-center" style={{ position: 'relative' }}>
          <p className="text-muted text-sm">
            © 2016 {author && author.companyName()}. All right reserved.
          </p>
        </div>*/}
      </div>
    )
  }

  render() {
    const { product } = this.props

    return (
      <div>
        {product ? this.renderProduct() : this.renderProductNotFound()
        }
      </div>
    )
  }
}

ProductCourseViewComponent.propTypes = {
  product: React.PropTypes.object,
  order: React.PropTypes.object,
  lessons: React.PropTypes.array
}

ProductCourseViewComponent.defaultProps = {

}

export default ProductCourseViewComponent
