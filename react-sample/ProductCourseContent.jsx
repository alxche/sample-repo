import React from 'react'
import { ProgressBar, Pager } from 'react-bootstrap'
import { Link } from 'react-router'
import { LinkContainer } from 'react-router-bootstrap'
import classNames from 'classnames'

import * as actions from '/imports/api/products/actions'

class ProductCourseContentComponent extends React.Component {

  constructor(props, { router }) {
    super(props)

    this.router = router

    this.state = {
    }
  }

  render() {
    const { currentUser } = this.context
    const { product, lesson } = this.props
    const treeData = product.tree()
    const prevLessonId = product.prevLesson(lesson._id)
    const nextLessonId = product.nextLesson(lesson._id)

    return (
      <div>
        <div className="white-bg p-10">
          <div className="bg-primary text-center" style={{ margin: '-10px -10px 10px -10px', padding: '20px 10px' }}>
            <i className="fa fa-graduation-cap fa-fw fa-2x" />
            <h3 className="m-none">{product.name}</h3>
          </div>
          <label>Current Progress</label>
          <ProgressBar now={currentUser.productProgress(product._id)} label={`${currentUser.productProgress(product._id)}%`} />
          {lesson &&
            <div className="m-t-10">
              <Pager>
                <LinkContainer to={this.router.paths.productCourseLessonView(product._id, prevLessonId)}>
                  <Pager.Item disabled={!prevLessonId}>
                    Previous
                  </Pager.Item>
                </LinkContainer>
                {' '}
                <LinkContainer to={this.router.paths.productCourseLessonView(product._id, nextLessonId)}>
                  <Pager.Item disabled={!nextLessonId}>
                    Next
                  </Pager.Item>
                </LinkContainer>
              </Pager>
            </div>
          }
          <ul className="list-group elements-list">
            {treeData && treeData.map(item => (
              <li className="list-group-item" key={item.id} style={{ padding: '15px 10px' }}>
                <Link
                  to={this.router.paths.productCourseLessonView(product._id, item.id)}
                  className={classNames({
                    active: this.router.params.lessonId === item.id,
                    'text-bold': this.router.params.lessonId === item.id
                  })}
                >
                  {/*<i className="fa fa-circle-o fs-12 m-r-5" />*/}
                  {item.title}
                </Link>
                <span className="pull-right">
                  <a onClick={() => actions.toggleLesson(item.id)}>
                    {currentUser.isLessonCompleted(item.id) ?
                      <i className="fa fa-check-square-o fa-fw fa-lg" /> :
                      <i className="fa fa-square-o fa-fw fa-lg" />
                    }
                  </a>
                </span>
                {(item.children && item.children.length) ?
                  <ul className="list-group elements-list" key={`children-${item.id}`}>
                    {item.children.map(child => (
                      <li className="list-group-item" key={child.id} style={{ padding: '15px 0 15px 10px', border: 0 }}>
                        <Link
                          to={this.router.paths.productCourseLessonView(product._id, child.id)}
                          className={classNames({
                            active: this.router.params.lessonId === child.id,
                            'text-bold': this.router.params.lessonId === child.id
                          })}
                        >
                          {child.title}
                        </Link>
                        <span className="pull-right">
                          <a onClick={() => actions.toggleLesson(child.id)}>
                            {currentUser.isLessonCompleted(child.id) ?
                              <i className="fa fa-check-square-o fa-fw fa-lg" /> :
                              <i className="fa fa-square-o fa-fw fa-lg" />
                            }
                          </a>
                        </span>
                      </li>
                    ))}
                  </ul> : null
                }
              </li>
            ))}
          </ul>
        </div>
      </div>
    )
  }
}

ProductCourseContentComponent.propTypes = {
  product: React.PropTypes.object,
  lesson: React.PropTypes.object,
  lessons: React.PropTypes.array
}

ProductCourseContentComponent.defaultProps = {

}

ProductCourseContentComponent.contextTypes = {
  router: React.PropTypes.object,
  currentUser: React.PropTypes.object
}

export default ProductCourseContentComponent
