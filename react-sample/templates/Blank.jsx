/* global Bert */
import React from 'react'
import { Form, Field } from 'simple-react-form'
import { TextField, EmailField, NumberField, SelectField, ObjectField } from 'simple-react-form-bootstrap'
import PhoneField from '/imports/lib/fields/PhoneField'

import { ProductPurchases } from '/imports/api/products'

import * as ProductActions from '/imports/api/products/actions'

class Component extends React.Component {

  constructor(props) {
    super(props)

    this.state = {
      order: {
        productId: props.product._id,
        shippingAddress: {
          country: 'us'
        },
        quantity: 1
      }
    }
  }

  render() {
    const { product } = this.props
    const { order } = this.state

    return (
      <div>
        <div className="sandbox-header">
          <h2>This product is in sandbox mode</h2>
          <p>No charges will be made, but you can test your upsell funnels</p>
        </div>
      </div>
    )
  }
}

Component.propTypes = {
  product: React.PropTypes.object
}

Component.defaultProps = {

}

export default Component
