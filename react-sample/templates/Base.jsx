/* global Bert */
import React from 'react'

class BaseComponent extends React.Component {

  constructor(props) {
    super(props)

    this.state = {
      order: {
        productId: props.product._id,
        shippingAddress: {
          country: 'us'
        },
        quantity: 1
      }
    }
  }
}

BaseComponent.propTypes = {
  product: React.PropTypes.object
}

BaseComponent.defaultProps = {

}

export default BaseComponent
