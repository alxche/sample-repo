import Classic from './Classic.jsx'
import CleanBlue from './CleanBlue.jsx'
import Showcase from './Showcase.jsx'
import SalesLetter from './SalesLetter.jsx'
import Marketer from './Marketer.jsx'
import Machine from './Machine.jsx'
import Formula from './Formula.jsx'
import BluePro from './BluePro.jsx'

export {
  Classic,
  CleanBlue,
  Showcase,
  SalesLetter,
  Marketer,
  Machine,
  Formula,
  BluePro
}
