    return (
      <div id="product-wrapper" className="product-classic">
      <div className="wrapper clearfix">
        <div className="wrapper-decoration">
          <header className="header">
            <div className="middle-container">
              <img className="logo" src="https://s3.amazonaws.com/samcart-foundation-prod/marketplace-5892/9FOVVG4FlEu93iktbJezONVn36qwTE.gif" />
              <div className="hide-on-mobile"><h1><span className="hide-help">Need Help? </span>support@quizlabs.co</h1></div>
            </div>
          </header>
          <div className="main-container">
            <div className="middle-container">
              <div className="about-product">
                <img className="prodct-image" src="https://s3.amazonaws.com/samcart-foundation-prod/marketplace-5892/XDsqT1bzsYGT7DmZdT8BLlwZFWyTsm.png" />
                <div>
                  <hgroup>
                    <h1 className="has-image">Fidget Cube - BUNDLE 3 Extra Cubes!</h1>
                    <div className="customize-text"><h2>Secure yourself a limited time offer of 3 ADDITIONAL Fidget Cubes in our Exclusive Color Combinations for $48. SAVE $11.97. Black/Green, Black/Black and Grey/Black combinations.</h2></div>
                  </hgroup>
                </div>
              </div>
              <div className="checkout-forms">
                {/* CHECKOUT CONTAINER LEFT SIDE */}
                <div className="col-md-7 checkout-input-container">
                  {/* CHECKOUT FORM */}
                  <div className="checkout-form">
                    <form id="paymentForm" name="paymentForm" className=" ">
                      <div className="section-title">
                        <h1>Contact Information</h1>
                        <p>Fields marked with <span>*</span> are required</p>
                      </div>
                      <input name="forced_sandbox" ng-model="forced_sandbox" defaultValue className="  " type="hidden" />
                      <input id="variation_id" name="variation_id" defaultValue={82605} type="hidden" />
                      {/* FIRST NAME, LAST NAME, ETC */}
                      <div className="formline two-parts">
                        <div className="col-md-6 left-input-column">
                          <label>First Name: <strong>*</strong></label>
                          <i className="fa fa-user" />
                          <input autofill id="fname" name="fname" className="required   " defaultValue ng-model="order.customer.first_name" ng-blur="captureProspect();" type="text" />
                        </div>
                        <div className="col-md-6 right-input-column">
                          <label>Last Name: <strong>*</strong></label>
                          <i className="fa fa-user" />
                          <input autofill id="lname" name="lname" className="required   " defaultValue ng-model="order.customer.last_name" ng-blur="captureProspect();" type="text" />
                        </div>
                      </div>
                      <div className="formline two-parts">
                        <div className="col-md-6 left-input-column">
                          <label>E-mail Address: <strong>*</strong></label>
                          <i className="fa fa-envelope-o" />
                          <input autofill id="email" name="email" className="required email   " defaultValue ng-model="order.customer.email" ng-blur="captureProspect();" type="text" />
                        </div>
                        <div className="col-md-6 right-input-column">
                          <label>Phone Number: <strong>*</strong></label>
                          <i className="fa fa-phone" />
                          <input autofill id="phone" name="phone" className="required phone   " defaultValue ng-model="order.customer.phone_number" ng-blur="captureProspect();" type="text" />
                        </div>
                      </div>
                      {/* END FIRST NAME, LAST NAME, ETC */}
                      {/* BILLING ADDRESS */}
                      {/* END BILLING ADDRESS */}
                      {/* SHIPPING ADDRESS */}
                      <div className="section-title top20">
                        <h1>Shipping Address</h1>
                        <p>Fields marked with <span>*</span> are required</p>
                      </div>
                      <div className="checkout-form">
                        <div className="formline col-md-12">
                          <label>Address: <strong>*</strong></label>
                          <i className="fa fa-map-marker" />
                          <input autofill id="shipping_address" name="shipping_address" className="required   " defaultValue ng-model="order.shipping_address.address" ng-blur="captureProspect();" type="text" />
                        </div>
                        <div className="formline col-md-12">
                          <label>Country: <strong>*</strong></label>
                          <i className="fa fa-globe" />
                          <i className="fa fa-caret-down select-arrow" />
                          <select name="shipping_country" id="shipping_country" ng-init="order.shipping_address.country = 643; countrySelected()" ng-model="order.shipping_address.country" ng-options="country.id as country.name for country in countries" ng-change="updateShippingCosts(82605, false, false)" ng-blur="captureProspect();" className="  ">
                            <option value>Select Country</option>
                          </select>
                        </div>
                        <div className="formline two-parts">
                          <div className="col-md-6 left-input-column">
                            <label>City: <strong>*</strong></label>
                            <i className="fa fa-building" />
                            <input autofill id="shipping_city" name="shipping_city" className="required   " defaultValue ng-model="order.shipping_address.city" ng-blur="captureProspect();" type="text" />
                          </div>
                          <div className="col-md-6 right-input-column" ng-show="order.shipping_address.country == 840">
                            <label>State: <strong>*</strong></label>
                            <i className="fa fa-flag" />
                            <i className="fa fa-caret-down select-arrow" />
                            <select id="shipping_state" name="shipping_state" ng-model="order.shipping_address.region" className="required   " ng-options="state.data as state.label for state in states" ng-blur="captureProspect();">
                              <option value>Select State</option>
                            </select>
                          </div>
                          <div className="col-md-6 right-input-column" ng-show="order.shipping_address.country != 840">
                            <label>Region: <strong>*</strong></label>
                            <i className="fa fa-flag" />
                            <input autofill id="shipping_region" name="shipping_address_region" className="required   " defaultValue ng-model="order.shipping_address.region" ng-blur="captureProspect();" type="text" />
                          </div>
                        </div>
                        <div className="formline two-parts">
                          <div className="col-md-6 left-input-column" ng-show="order.shipping_address.country == 840">
                            <label>Zip Code: <strong>*</strong></label>
                            <i className="fa fa-map-marker" />
                            <input autofill id="shipping_zip" name="shipping_address_zip" className="required   " defaultValue ng-model="order.shipping_address.postal" ng-blur="captureProspect();" type="text" />
                          </div>
                          <div className="col-md-6 left-input-column" ng-show="order.shipping_address.country != 840">
                            <label>Postal Code: <strong>*</strong></label>
                            <i className="fa fa-map-marker" />
                            <input autofill id="shipping_postal_code" name="shipping_address_postal_code" defaultValue ng-model="order.shipping_address.postal" ng-blur="captureProspect();" className="  " type="text" />
                          </div>
                        </div>
                      </div>
                      {/* END SHIPPING ADDRESS */}
                      {/* COUPON */}
                      {/* END COUPON */}
                      {/* PAYMENT SECTION */}
                      <div className="order-payment">
                        <div className="payment-form">
                          <span ng-init="processor_selection='cc_processor'" />
                          <div className="carts_options">
                            <div className="payment-selection">
                              <label>
                                <input name="cart_select" ng-click="setPaymentType('cc_processor')" defaultChecked type="radio" />
                                <span className="icon" />
                                <span className="label"><img className="selection-img" src="https://samcart-foundation-prod.s3.amazonaws.com/templates/sunshine/img/icons/carts.png" /></span>
                              </label>
                              <label>
                                <input name="cart_select" ng-click="setPaymentType('paypal')" type="radio" />
                                <span className="icon" />
                                <span className="label"><img className="selection-img paypal" src="https://samcart-foundation-prod.s3.amazonaws.com/templates/sunshine/img/icons/paypal.png" /></span>
                              </label>
                            </div>
                          </div>
                          <div className="section-title">
                            <h1>Payment Information</h1>
                            <p>All fields are required</p>
                          </div>
                          {/* MULTIPLE PAYMENTS */}
                          {/* END MULTIPLE PAYMENTS */}
                          <span ng-init="old_paypal=false" />
                          {/* ngIf: processor_selection == 'cc_processor' */}
                        </div>
                        {/* If the product is not a physical object, show order-bumps*/}
                        {/* If the product IS physical, order bumps will show up in the physical product blade */}
                        <div className="place-order col-md-12">
                          <div className="total-amount order_total">
                            <div className="price">
                              <div className="row">
                                <div className="col-md-4">
                                  <span>Subtotal:</span>
                                </div>
                                <div className="price-placement single-product notranslate center-price-text">$<div className="product-price product-subtotal">48.00</div></div>
                              </div>
                            </div>
                            {/* Quantity | Shipping | Tax */}
                            <div className="row">
                              <div className="quantity-container">
                                <div className="formline">
                                  <div className="col-md-6">
                                    <label>Quantity: *</label>
                                    <i className="fa fa-caret-down select-arrow" />
                                    <select data-stripe="quantity" id="quantity" name="quantity" ng-model="order.quantity" className="required   " ng-change="updateShippingCosts(82605, false, false)">
                                      <option value={1}>1</option>
                                      <option value={2}>2</option>
                                      <option value={3}>3</option>
                                      <option value={4}>4</option>
                                      <option value={5}>5</option>
                                      <option value={6}>6</option>
                                      <option value={7}>7</option>
                                      <option value={8}>8</option>
                                      <option value={9}>9</option>
                                      <option value={10}>10</option>
                                    </select>
                                  </div>
                                  <div className="col-md-6">
                                    <div className="field-wrap">
                                      <label>Shipping:</label>
                                      <div className="field-container">
                                        <div className="price-placement pp-shipping">
                                          $<div id="shipping-price" className="product-price ng-binding">8.98</div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              {/* ORDER BUMP */}
                              {/* ORDER BUMP START */}
                              <div className="col-md-12">
                                <div className="price">
                                  <div className="row">
                                    <div className="col-md-4">
                                      <span>Total:</span>
                                    </div>
                                    <div className="price-placement single-product notranslate center-price-text" ng-show="!show_loading_totals">$<div className="product-price grand-total" ng-show="!show_loading_totals">60.97</div></div>
                                  </div>
                                  <div className="update-total-loading" ng-show="show_loading_totals"><i className="fa fa-spin fa-spinner" /></div>
                                </div>
                              </div>
                            </div>
                            <img src="https://samcart-foundation-prod.s3.amazonaws.com/templates/shared/img/trust-seals.png" className="trust-seals resize" width="75%" />
                            <button type="submit" ng-model="buttonWithoutTerms" className="main-cta resize   " id="placeOrder" name="placeOrder" ng-click="placeOrder('D5PggvNi4ujgLDIdMkYzQF7z')">Place Order Now</button>
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
                {/* END CHECKOUT CONTAINER LEFT SIDE */}
                {/* CHECKOUT CONTAINER RIGHT SIDE */}
                <div className="col-md-5">
                  <div className="trust-panel whats-inside hide-991">
                    <p>Secure yourself a limited time offer of 3 ADDITIONAL Fidget Cubes in our Exclusive Color Combinations for $48. SAVE $11.97 You will receive 3 EXTRA Fidget Cubes in Black/Green, Black/Black and Grey/Black combinations.</p>
                  </div>{/* close trust-panel */}
                  <div className="three-sections hide-991">
                    <table>
                      <tbody>
                        {/* PRIVACY ICONS */}
                        <tr>
                          {/* GUARANTEE */}
                          <td className="cell01 symbol03">
                            <div className="relative-wrapper">
                              <img className="noguarantee" src="/img/aes256.png" />
                              <p>AES-256bit<br />Encryption</p>
                              <div className="tooltip">
                                <div className="tooltip-inner">
                                  <em>Secure transaction to ensure your information is safe.</em>
                                  <i className="fa fa-caret-down" />
                                </div>
                              </div>
                            </div>
                          </td>
                          <td className="cell02 symbol02">
                            <div className="relative-wrapper">
                              <span />
                              <p>QuizLabs<br />Protects Your<br />Privacy</p>
                              <div className="tooltip">
                                <div className="tooltip-inner">
                                  <em>We will not share or trade online information that you provide us.</em>
                                  <i className="fa fa-caret-down" />
                                </div>
                              </div>
                            </div>
                          </td>
                          <td className="cell03 symbol03">
                            <div className="relative-wrapper">
                              <span />
                              <p>Your<br />Information<br />is Secure</p>
                              <div className="tooltip">
                                <div className="tooltip-inner">
                                  <em>All personal information is encrypted and secure.</em>
                                  <i className="fa fa-caret-down" />
                                </div>
                              </div>
                            </div>
                          </td>
                        </tr>
                        {/* END PRIVACY ICONS */}
                      </tbody>
                    </table>
                  </div>
                </div>
                {/* CHECKOUT CONTAINER RIGHT SIDE */}
              </div>
            </div>
          </div>
          <footer className="footer">
            <div className="footer-decoration">
              <div className="middle-container">
                <img className="logo" src="https://s3.amazonaws.com/samcart-foundation-prod/marketplace-5892/9FOVVG4FlEu93iktbJezONVn36qwTE.gif" />
                <div>
                  <p>Copyright © 2017 QuizLabs - All Rights Reserved</p>
                </div>
              </div>
            </div>
          </footer>
        </div>
      </div>
      </div>
    )
