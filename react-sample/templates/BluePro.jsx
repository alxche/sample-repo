/* global Bert */
import React from 'react'
import { Form, Field } from 'simple-react-form'
import { TextField, EmailField, NumberField, SelectField, ObjectField } from 'simple-react-form-bootstrap'
import PhoneField from '/imports/lib/fields/PhoneField'
import InputMaskField from '/imports/lib/fields/InputMask'

import {
  eventProductCheckoutPageView,
  eventProductSalesPageView,
  eventProductCheckoutPageAbandon
} from '/imports/api/analytics/events'

import * as ProductActions from '/imports/api/products/actions'
import { withPurchase } from '/imports/ui/decorators'

@withPurchase
class BlueProComponent extends React.Component {
  render() {
    const { product } = this.props
    const { order } = this.state
    const reward = product.reward()

    return (
      <div id="product-wrapper" className="blue-pro">
        {!product.active &&
          <div className="sandbox-header">
            <h2>This product is in sandbox mode</h2>
            <p>No charges will be made, but you can test your sales funnel</p>
          </div>
        }
        <div className="p-wrapper">
          <div className="p-header" style={{ backgroundColor: product.headerColor, color: product.textColor }}>
            <div className="container">
              {product.companyLogo &&
                <div className="logo">
                  <img src={product.companyLogo} className="company-logo" />
                </div>
              }
              {product.companyPhone && <p className="help-text">Need Help? {product.companyPhone}</p>}
            </div>
          </div>
          <div className="cl-backg" style={{ backgroundColor: product.backgroundColor }}></div>
          <div className="container p-container">
            <div className="ui segment">
              <div className="p-info">
                <div className="row">
                  <div className="col-md-6">
                    {product.image && <img className="img-responsive product-image" src={product.image} />}
                  </div>
                  <div className="col-md-6">
                    <h1 className="bp-banner-bigtext text-right" style={{ color: product.headlineColor }}>
                      {product.name}
                    </h1>
                  </div>
                </div>
                <div className="fr-view" dangerouslySetInnerHTML={{ __html: product.customContent }} />
                <div className="p-bullet-points list-wrapper">
                  {product.bulletPointsTitle && <h3 className="list-title text-center">{product.bulletPointsTitle}</h3>}
                  <ul className="sidebar-list">
                    {product.bulletPoints && product.bulletPoints.map((bullet, index) => (
                      <li
                        key={`bulletPoints${index}`}
                        style={{
                        }}
                      >
                        {product.bulletPointsIcon ?
                          <i className={`fa ${product.bulletPointsIcon} fa-lg fa-fw icon`} style={{ color: product.bulletPointsIconColor }} /> :
                          <img className="img-icon" src={product.bulletPointsImage} />
                        }
                        <span className="bullet-text" style={{ color: product.bulletPointsTextColor }}>{bullet.text}</span>
                      </li>
                    ))}
                  </ul>
                </div>
                <div className="p-testimonials testimonial-group">
                  {product.testimonialsTitle && <h3>{product.testimonialsTitle}</h3>}
                  {product.testimonials && product.testimonials.map((testimonial, index) => (
                    <div className="testimonial-item" key={`testimonial${index}`}>
                      {testimonial.avatar &&
                        <div className="testimonial-image">
                          <img src={testimonial.avatar} />
                        </div>
                      }
                      <div className="testimonial-text">
                        <strong>{testimonial.name}</strong>
                        <p><em>{testimonial.text}</em></p>
                      </div>
                    </div>
                  ))}
                </div>
              </div>
              <Form
                doc={order}
                schema={product.purchaseSchema()}
                type="function"
                onSubmit={this.onSubmit}
                onChange={charges => this.setState({ order: charges })}
              >
                <div>
                  <div className="section-header">
                    <h3>Contact Information</h3>
                  </div>
                  <div className="row">
                    <div className="col-sm-6">
                      <Field fieldName="firstName" type={TextField} label="First Name" />
                    </div>
                    <div className="col-sm-6">
                      <Field fieldName="lastName" type={TextField} label="Last Name" />
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-sm-6">
                      <Field fieldName="email" type={EmailField} label="Email Address" />
                    </div>
                    <div className="col-sm-6">
                      {product.enablePhoneNumber === false ?
                        <div /> :
                        <Field fieldName="phone" type={PhoneField} label="Phone Number" />
                      }
                    </div>
                  </div>
                </div>
                {product.isShipping() &&
                  <div>
                    <div className="section-header">
                      <h3>Shipping Address</h3>
                    </div>
                    <Field fieldName="shippingAddress" type={ObjectField} showLabel={false}>
                      <Field fieldName="line1" type={TextField} label="Address 1" />
                      <Field fieldName="line2" type={TextField} label="Address 2" />
                      <div className="row">
                        <div className="col-sm-4">
                          <Field fieldName="zip" type={TextField} />

                        </div>
                        <div className="col-sm-4">
                          <Field fieldName="city" type={TextField} />
                        </div>
                        <div className="col-sm-4">
                          {(order.shippingAddress && order.shippingAddress.country === 'us') ?
                            <Field fieldName="state" type={SelectField} /> :
                            <Field fieldName="county" type={TextField} />
                          }
                        </div>
                      </div>
                      <Field fieldName="country" type={SelectField} />
                    </Field>
                  </div>
                }
                {product.enableBillingAddress &&
                  <div>
                    <div className="section-header">
                      <h3>Billing Address</h3>
                    </div>
                    <Field fieldName="billingAddress" type={ObjectField} showLabel={false}>
                      <Field fieldName="line1" type={TextField} label="Address 1" />
                      <Field fieldName="line2" type={TextField} label="Address 2" />
                      <div className="row">
                        <div className="col-sm-4">
                          <Field fieldName="zip" type={TextField} />

                        </div>
                        <div className="col-sm-4">
                          <Field fieldName="city" type={TextField} />
                        </div>
                        <div className="col-sm-4">
                          {(order.billingAddress && order.billingAddress.country === 'us') ?
                            <Field fieldName="state" type={SelectField} /> :
                            <Field fieldName="county" type={TextField} />
                          }
                        </div>
                      </div>
                      <Field fieldName="country" type={SelectField} />
                    </Field>
                  </div>
                }
                {this.renderGateways()}
              </Form>
            </div>
          </div>
          <footer className="p-footer" style={{ backgroundColor: product.footerColor, color: product.textColor }}>
            <div className="decoration">
              <div className="middle-container">
                <div>
                  <p>Copyright &copy; 2016 &nbsp;
                    <a href={product.companyUrl} style={{ color: 'inherit' }} target="_blank">
                      {product.companyName}
                    </a>
                  </p>
                </div>
              </div>
            </div>
          </footer>
        </div>
      </div>
    )
  }
}

BlueProComponent.propTypes = {
  product: React.PropTypes.object
}

BlueProComponent.defaultProps = {

}

export default BlueProComponent
