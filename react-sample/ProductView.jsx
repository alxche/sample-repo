/* global Bert */
import React from 'react'

class ProductViewComponent extends React.Component {

  constructor(props) {
    super(props)

    this.state = {
    }
  }

  render() {
    const { product, order } = this.props

    return (
      <div className="container">
        <h1>{product.name}</h1>
        <div className="row">
          <div className="col-sm-6">
            <h2>Total:</h2>
          </div>
          <div className="col-sm-6">
            <h2>{order.totalFormatted()}</h2>
          </div>
        </div>
      </div>
    )
  }
}

ProductViewComponent.propTypes = {
  product: React.PropTypes.object,
  order: React.PropTypes.object
}

ProductViewComponent.defaultProps = {

}

export default ProductViewComponent
