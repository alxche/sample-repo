/* global Bert */
import React from 'react'
import { Form, Field } from 'simple-react-form'
import { TextField, SelectField } from 'simple-react-form-bootstrap'

class ProductPhysicalViewComponent extends React.Component {

  constructor(props) {
    super(props)

    this.state = {
    }
  }

  render() {
    const { product, order } = this.props

    return (
      <div className="container">
        <div className="ui segment m-t-50">
          <h1>{product.name}</h1>
          <div className="row">
            <div className="col-sm-6">
              <h2>Total:</h2>
            </div>
            <div className="col-sm-6">
              <h2>{order && order.totalFormatted()}</h2>
            </div>
          </div>
        </div>
        <div className="footer text-center m-t-30" style={{ position: 'relative' }}>
          <p className="text-muted text-13">
            Copyright &copy; 2017 {product.companyName}. All right reserved.
          </p>
        </div>
      </div>
    )
  }
}

ProductPhysicalViewComponent.propTypes = {
  product: React.PropTypes.object,
  order: React.PropTypes.object
}

ProductPhysicalViewComponent.defaultProps = {

}

export default ProductPhysicalViewComponent
