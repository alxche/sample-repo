/* global Bert */
import React from 'react'
import { Button } from 'react-bootstrap'
import { Form, Field } from 'simple-react-form'
import { TextField, SelectField, RadioField, ArrayField } from 'simple-react-form-bootstrap'

import { Products } from '/imports/api/products'

import ProductTopBanner from './ProductTopBanner'
import ProductMenu from './ProductMenu'

class ProductTypeComponent extends React.Component {

  constructor(props, { router }) {
    super(props)

    this.renderShippingItem = this.renderShippingItem.bind(this)

    this.router = router

    this.state = {
      product: props.product
    }
  }

  onSuccess() {
    const { product } = this.props
    Bert.alert('Success', 'success')
    this.router.push(this.router.paths.productDetails(product._id))
  }

  renderShippingItem(item) {
    return (
      <div className="row">
        <div className="col-sm-4">
          <Field fieldName="country" type={SelectField} label="" placeholder="Ships to" />
        </div>
        <div className="col-sm-4">
          <Field fieldName="amount" type={TextField} label="" placeholder="By itself" />
        </div>
        <div className="col-sm-4">
          <Field fieldName="anotherAmount" type={TextField} label="" placeholder="With Another Item" />
        </div>
      </div>
    )
  }

  render() {
    const { product } = this.state

    return (
      <div>
        <div className="border-bottom white-bg p-t-10 p-b-10">
          <div className="container-fluid">
            <ProductTopBanner product={product} />
          </div>
        </div>
        <div className="ibox float-e-margins">
          <div className="ibox-content">
            <ProductMenu product={product} />
            <div className="m-t-20">
              <Form doc={product} collection={Products} type="update" onChange={changes => this.setState({ product: changes })} onSuccess={() => this.onSuccess()}>
                <p>What type of product are you selling is it physical or digital only. We call the products either physical or courses. A physical product will require a shipping address where that is optional for the digital courses.</p>
                <Field fieldName="type" type={RadioField} />
                {product.type === 'physical' &&
                  <div>
                    <Field fieldName="shippingAddresses" type={ArrayField} label="Ships to" renderItem={this.renderShippingItem} />
                  </div>
                }
                <Button bsStyle="primary" type="submit">Save Product</Button>
              </Form>
              <br />
              <br />
              <h2>Sandbox Mode</h2>
              <p>
                This allows you to test your sales funnel without the need for a real credit card. This allow you to confirm everything is working the way it should be, email fire, up-sells are correct. You should not provide you link to anyone who you want to buy as the order will be created however there will be no Real payment processed. When you are ready to go live and process payment you need to switch to “Active” mode. Sandbox is draft mode, not ready for the real world yet. Use this credit card number when testing checkout “4242 4242 4242 4242” and use any valid date along with any 3 digits for CVV.
                <strong>You cannot use a REAL credit cart when testing checkout. Must use the test credit card number</strong>
              </p>
              <h2>Active Mode</h2>
              <p>This means your system is now ready to process LIVE transactions. It is recommended that you complete at least one LIVE transaction test to confirm your payment processor is connected correctly. This confirms when your customers checkout that payment works, emails are received and they can access their purchase. Active means you are ready to start selling to the world.</p>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

ProductTypeComponent.propTypes = {
  product: React.PropTypes.object
}

ProductTypeComponent.defaultProps = {

}

ProductTypeComponent.contextTypes = {
  router: React.PropTypes.object
}

export default ProductTypeComponent
