import ProductCreate from './ProductCreate.jsx'
import Products from './Products.jsx'
import ProductType from './ProductType.jsx'
import ProductLessons from './ProductLessons.jsx'
import ProductLessonEdit from './ProductLessonEdit.jsx'
import ProductStructureLessons from './ProductStructureLessons.jsx'
import ProductCustomize from './ProductCustomize.jsx'
import ProductDetails from './ProductDetails.jsx'
import ProductIntegrations from './ProductIntegrations.jsx'
import ProductAdvanced from './ProductAdvanced.jsx'
import ProductCoupons from './ProductCoupons.jsx'
import ProductVariations from './ProductVariations.jsx'
import ProductSocialMetaData from './ProductSocialMetaData.jsx'
import ProductLibrary from './ProductLibrary.jsx'
import ProductPhysicalView from './ProductPhysicalView.jsx'
import ProductCourseView from './ProductCourseView.jsx'
import ProductCourseLessonView from './ProductCourseLessonView.jsx'
import ProductAnalytics from './ProductAnalytics.jsx'
import Rewards from './Rewards.jsx'
import RewardEdit from './RewardEdit.jsx'
import RewardRules from './RewardRules.jsx'
import Orders from './Orders.jsx'
import OrderCreate from './OrderCreate.jsx'
import OrderView from './OrderView.jsx'
import OrderEdit from './OrderEdit.jsx'
import Customers from './Customers.jsx'
import CustomerView from './CustomerView.jsx'
import CustomerOrders from './CustomerOrders.jsx'
//import CustomerEdit from './OrderEdit.jsx'

export {
  ProductCreate,
  Products,
  ProductType,
  ProductLessons,
  ProductStructureLessons,
  ProductLessonEdit,
  ProductCustomize,
  ProductDetails,
  ProductIntegrations,
  ProductAdvanced,
  ProductCoupons,
  ProductVariations,
  ProductPhysicalView,
  ProductCourseView,
  ProductCourseLessonView,
  ProductSocialMetaData,
  ProductAnalytics,
  ProductLibrary,
  Rewards,
  RewardEdit,
  RewardRules,
  Orders,
  OrderCreate,
  OrderView,
  OrderEdit,
  Customers,
  CustomerView,
  CustomerOrders
}
