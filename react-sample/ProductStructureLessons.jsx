/* global Bert */
import React from 'react'
import { Button } from 'react-bootstrap'
import SortableTree from 'react-sortable-tree'

import { Products } from '/imports/api/products'

import { resetLessonStructure } from '/imports/api/products/actions'

import ProductTopBanner from './ProductTopBanner'
import ProductMenu from './ProductMenu'

class ProductStructureLessonsComponent extends React.Component {

  constructor(props) {
    super(props)

    this.onChange = this.onChange.bind(this)

    this.state = {
      treeData: props.product.tree()
    }
  }

  componentWillReceiveProps({ product }) {
    this.setState({ treeData: product.tree() })
  }

  onChange(treeData) {
    const { product } = this.props
    Products.direct.update(product._id, {
      $set: { structureLessons: treeData }
    })
    this.setState({ treeData })
  }

  render() {
    const { product } = this.props
    const { treeData } = this.state

    return (
      <div>
        <div className="border-bottom white-bg p-t-10 p-b-10">
          <div className="container-fluid">
            <ProductTopBanner product={product} />
          </div>
        </div>
        <div className="ibox float-e-margins">
          <div className="ibox-content">
            <ProductMenu product={product} />
            <p className="space-10">This allows you to order your course or your lesson plan. You can drag and drop lessons and layer lessons. This instantly applies this structure to the members area.</p>
            <div className="m-t-20">
              <div className="" style={{ height: 600, width: '100%' }}>
                <SortableTree
                  treeData={treeData}
                  maxDepth={3}
                  getNodeKey={({ node }) => node.id}
                  onChange={this.onChange}
                />
              </div>
              <Button bsStyle="primary" onClick={() => resetLessonStructure(product._id)}>Reset Structure</Button>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

ProductStructureLessonsComponent.propTypes = {
  product: React.PropTypes.object,
  lessons: React.PropTypes.array
}

ProductStructureLessonsComponent.defaultProps = {

}

export default ProductStructureLessonsComponent
