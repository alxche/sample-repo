/* global Bert */
import _ from 'underscore'
import React from 'react'
import { Form, Field } from 'simple-react-form'
import Clipboard from 'react-copy-to-clipboard'

import ToggleField from '/imports/lib/fields/Toggle'

import { Products } from '/imports/api/products'

class ProductTopBannerComponent extends React.Component {

  constructor(props) {
    super(props)

    this.state = {
    }
  }

  render() {
    const { product } = this.props

    return (
      <div className="row">
        <div className="col-md-8">
          <div className="top-banner form-inline">
            <div className="form-group">
              <label className="m-r-5">Product URL</label>
              <input type="text" className="form-control w-400 inline-block" value={product.url()} readOnly />
            </div>
            <div className="form-group">
              <Clipboard text={product.url()} onCopy={() => Bert.alert('Copied', 'success', 'fixed-top')}>
                <button className="btn btn-primary">Copy URL</button>
              </Clipboard>
            </div>
          </div>
        </div>
        <div className="col-md-4 text-right">
          <Form doc={product} collection={Products} type="update" className="product-top-banner form-inline" autoSave>
            <Field fieldName="active" type={ToggleField} labelOff="Sandbox" labelOn="Live" />
            <a href={product.url()} className="btn btn-default" target="_blank">
              <i className="fa fa-eye fa-fw" />
            </a>
          </Form>
        </div>
      </div>
    )
  }
}

ProductTopBannerComponent.propTypes = {
  product: React.PropTypes.object
}

ProductTopBannerComponent.defaultProps = {

}

export default ProductTopBannerComponent
