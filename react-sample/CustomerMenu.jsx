import React from 'react'
import { Nav, NavItem } from 'react-bootstrap'
import { LinkContainer } from 'react-router-bootstrap'

class CustomerMenuComponent extends React.Component {

  constructor(props) {
    super(props)

    this.state = {
    }
  }

  render() {
    const { router } = this.context
    const { customer } = this.props
    const customerId = customer._id

    return (
      <Nav bsStyle="pills" className="nav-secondary" activeHref={router.location.pathname}>
        <LinkContainer to={router.paths.customerView(customerId)}>
          <NavItem>View</NavItem>
        </LinkContainer>
        <LinkContainer to={router.paths.customerOrders(customerId)}>
          <NavItem>Purchases</NavItem>
        </LinkContainer>
      </Nav>
    )
  }
}

CustomerMenuComponent.propTypes = {
  customer: React.PropTypes.object
}

CustomerMenuComponent.defaultProps = {

}

CustomerMenuComponent.contextTypes = {
  router: React.PropTypes.object
}

export default CustomerMenuComponent
