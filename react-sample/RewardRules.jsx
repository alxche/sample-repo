/* global Bert */
import { Memberships } from 'meteor/rgnevashev:memberships'

import React from 'react'
import { Form, Field } from 'simple-react-form'
import { TextField, NumberField, SelectField, ArrayField } from '/imports/lib/srf/fields'

import { Rewards, Products } from '/imports/api/products'

import RewardMenu from './RewardMenu'

class RewardRulesComponent extends React.Component {

  constructor(props) {
    super(props)

    this.onSuccess = this.onSuccess.bind(this)
    this.renderRule = this.renderRule.bind(this)

    this.state = {
      reward: props.reward
    }
  }

  onSuccess() {
    Bert.alert('Reward Rules saved', 'success', 'fixed-top', 'fa-smile-o')
  }

  ruleTypeOptions() {
    let options = []
    if (Memberships.hasRole('admin')) {
      options = [
        { label: 'Choose Type', value: '' },
        { label: 'QL Trial', value: 'QL-Trial' },
        { label: 'Add Access Product', value: 'Product-Access' },
        { label: 'Add Trial Product', value: 'Product-Trial' },
        { label: 'Add Discount Product', value: 'Product-Discount' }
      ]
    } else {
      options = [
        { label: 'Choose Type', value: '' },
        { label: 'Add Access Product', value: 'Product-Access' },
        { label: 'Add Trial Product', value: 'Product-Trial' },
        { label: 'Add Discount Product', value: 'Product-Discount' }
      ]
    }
    return options
  }

  productOptions(selector = {}) {
    const options = (Products.find({ ...selector, owner: Meteor.userId() }).map(product => (
      { label: `${product.name} - ${product.costFormatted({})}`, value: product._id }
    ))) || []
    options.unshift({ label: 'Choose Product', value: '' })
    return options
  }

  discountTypeOptions() {
    return [
      { label: 'Choose Discount Type', value: '' },
      { label: '%', value: 'percent' },
      { label: '$', value: 'amount' }
    ]
  }

  selectedProduct(rule) {
    return Products.findOne(rule && rule.params && rule.params.productId)
  }

  renderRule(rule) {
    const product = this.selectedProduct(rule)

    return (
      <div>
        <Field fieldName="type" type={SelectField} options={this.ruleTypeOptions()} />
        {rule.type === 'QL-Trial' &&
          <div>
            <Field fieldName="params.trialDays" type={NumberField} label="QL Trial in Days" />
          </div>
        }
        {rule.type === 'Product-Access' &&
          <div>
            <Field fieldName="params.productId" type={SelectField} label="Product" options={this.productOptions()} selectable />
            {product && product.isPackages() &&
              <Field fieldName="params.packageIds" type={SelectField} label="Package" placeholder="Any Packages" options={product.packageOptions(false)} selectable multi />
            }
            {product && product.isRecurring() &&
              <Field fieldName="params.planIds" type={SelectField} label="Plan" placeholder="Any Plans" options={product.planOptions(false)} selectable multi />
            }
          </div>
        }
        {rule.type === 'Product-Trial' &&
          <div>
            <Field
              fieldName="params.productId"
              type={SelectField}
              label="Product"
              options={this.productOptions({})} // { priceType: { $in: ['level', 'recurring'] } }
              selectable
            />
            {/*<Field fieldName="params.trialAmount" type={TextField} label="Trial Amount" placeholder="0" forceLabel />*/}
            <Field fieldName="params.trialDays" type={NumberField} label="Trial in Days" />
          </div>
        }
        {rule.type === 'Product-Discount' &&
          <div>
            <Field
              fieldName="params.productId"
              type={SelectField}
              label="Product"
              options={this.productOptions({})}
              selectable
            />
            <div className="row">
              <div className="col-sm-2">
                <Field fieldName="params.discountType" type={SelectField} label="Discount Type" options={this.discountTypeOptions()} />
              </div>
              <div className="col-sm-10">
                <Field fieldName="params.discount" type={NumberField} label="Discount" />
              </div>
            </div>
          </div>
        }
      </div>
    )
  }

  render() {
    const { reward } = this.state

    return (
      <div>
        <div className="border-bottom white-bg p-t-10 p-b-10">
          <div className="container-fluid">
            <h2>Reward Edit</h2>
            <span>Sub.</span>
          </div>
        </div>
        <div className="ibox float-e-margins">
          <div className="ibox-content">
            <RewardMenu reward={reward} />
            <div className="m-t-20">
              <Form
                doc={reward}
                collection={Rewards}
                type="update"
                onChange={changes => this.setState({ reward: changes })}
                onSuccess={() => this.onSuccess()}
              >
                <Field fieldName="name" type={TextField} label="Name" />
                <Field fieldName="rules" type={ArrayField} renderItem={this.renderRule} autoAddItem />
                <button className="btn btn-primary" type="submit">Save</button>
              </Form>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

RewardRulesComponent.propTypes = {
  reward: React.PropTypes.object
}

RewardRulesComponent.defaultProps = {

}

export default RewardRulesComponent
