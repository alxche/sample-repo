/* global Bert: true */
import React from 'react'
import { Form, Field } from 'simple-react-form'
import { TextField, ObjectField } from 'simple-react-form-bootstrap'

import { Products } from '/imports/api/products'

import ProductTopBanner from './ProductTopBanner'
import ProductMenu from './ProductMenu'

class ProductAnalytics extends React.Component {

  constructor(props) {
    super(props)

    this.onChange = this.onChange.bind(this)
    this.onSuccess = this.onSuccess.bind(this)
    this.verifyAnalytics = this.verifyAnalytics.bind(this)

    this.state = {
      product: props.product
    }
  }

  onChange(product) {
    this.setState({ product })
  }

  onSuccess() {
    Bert.alert('Product Analytics saved', 'success', 'fixed-top', 'fa-smile-o')
  }

  verifyAnalytics() {
    const { product } = this.state
    const trackingId = product.analytics && product.analytics['Google Analytics'] && product.analytics['Google Analytics'].trackingId
    const pixelId = product.analytics && product.analytics['Facebook Pixel'] && product.analytics['Facebook Pixel'].pixelId
    const isValidTrackingId = /\bUA-\d{4,10}-\d{1,4}\b/i.test(trackingId) // \bUA-\d{4,10}-\d{1,4}\b    (UA|YT|MO)-\d+-\d+
    const isValidPixelId = /^\d+$/i.test(pixelId)

    if (isValidPixelId && isValidTrackingId) {
      Bert.alert('Analytics ID\'s are valid', 'success', 'fixed-top', 'fa-smile-o')
    } else if (!isValidTrackingId) {
      Bert.alert('Google Analytics ID is invalid', 'warning', 'fixed-top', 'fa-frown-o')
    } else if (!isValidPixelId) {
      Bert.alert('Facebook Pixel ID is invalid', 'warning', 'fixed-top', 'fa-frown-o')
    }
  }

  render() {
    const { product } = this.state
    const { currentUser } = this.context

    return (
      <div>
        <div className="border-bottom white-bg p-t-10 p-b-10">
          <div className="container-fluid">
            <ProductTopBanner product={product} />
          </div>
        </div>
        <div className="ibox float-e-margins">
          <div className="ibox-content">
            <ProductMenu product={product} />
            <div className="m-t-20">
              <p><a href="https://productlabs.co/a/setup-your-product-analytics" target="_blank">Cick here</a> for more information on setting up your Analytics</p>
              <Form doc={product} collection={Products} type="update" onSuccess={this.onSuccess} onChange={this.onChange}>
                <Field fieldName="analytics" type={ObjectField} showLabel={false} >
                  <Field fieldName="Google Analytics" type={ObjectField} showLabel={false} >
                    <Field fieldName="trackingId" type={TextField} label="Google Analytics Id" value={product.googleAnalyticsId() || currentUser.googleAnalyticsId()} />
                    <p className="text-muted m-t-n-15">
                      <small>Insert your Google Analytics ID number. This will be the "UA-" code followed by digits. For example "UA-83124849-1" found inside your Google Analytics console.</small>
                    </p>
                  </Field>
                  <Field fieldName="Facebook Pixel" type={ObjectField} showLabel={false} >
                    <Field fieldName="pixelId" type={TextField} label="Facebook Pixel Id" value={product.facebookPixelId() || currentUser.facebookPixelId()} />
                    <p className="text-muted m-t-n-15">
                      <small>Insert only your Facebook Pixel ID number. This is generated inside your Facebook advertising account and will be a numeric number. You do not need to insert the script just your Pixel ID number.</small>
                    </p>
                  </Field>
                </Field>
                <button className="btn btn-primary" type="submit">Save</button>
                <button className="btn btn-warning" type="button" onClick={this.verifyAnalytics}>Verify</button>
              </Form>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

ProductAnalytics.propTypes = {
  productId: React.PropTypes.string,
  product: React.PropTypes.object
}

ProductAnalytics.contextTypes = {
  currentUser: React.PropTypes.object
}

export default ProductAnalytics
