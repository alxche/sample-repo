import React from 'react'
import { LinkContainer } from 'react-router-bootstrap'
import { ButtonGroup, Button } from 'react-bootstrap'
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table'

import CustomerMenu from './CustomerMenu.jsx'

class CustomerOrdersComponent extends React.Component {

  constructor(props, { router }) {
    super(props)

    this.actionsFormatter = this.actionsFormatter.bind(this)

    this.router = router

    this.state = {
    }
  }

  actionsFormatter(orderId) {
    return (
      <ButtonGroup>
        <LinkContainer to={this.router.paths.orderView(orderId)}>
          <Button>
            <i className="fa fa-eye fa-fw" />
          </Button>
        </LinkContainer>
        <LinkContainer to={this.router.paths.orderEdit(orderId)}>
          <Button>
            <i className="fa fa-edit fa-fw" />
          </Button>
        </LinkContainer>
      </ButtonGroup>
    )
  }

  render() {
    const { customer, orders } = this.props
    const options = {
      defaultSortName: 'createdAt',
      defaultSortOrder: 'desc',
      sizePerPage: 25,
      noDataText: 'There are no customers created yet.'
    }

    return (
      <div>
        <div className="border-bottom white-bg p-20">
          <div className="container-fluid">
            <h1 className="m-none">Customer Purchases</h1>
          </div>
        </div>
        <div className="ibox float-e-margins">
          <div className="ibox-content">
            <div className="m-t-20">
              <CustomerMenu customer={customer} />
              <BootstrapTable data={orders} options={options} striped hover pagination search>
                <TableHeaderColumn dataField="purchasedAt" width="150" dataSort>Order Date</TableHeaderColumn>
                <TableHeaderColumn dataField="productName" width="200" dataSort>Product</TableHeaderColumn>
                <TableHeaderColumn dataField="email" width="250" dataSort>Customer Email</TableHeaderColumn>
                <TableHeaderColumn dataField="statusFormatted" width="120" dataSort>Order Status</TableHeaderColumn>
                <TableHeaderColumn dataField="totalFormatted" width="100" dataSort>Order Total</TableHeaderColumn>
                <TableHeaderColumn dataField="paymentSource" width="120" dataSort>Payment Source</TableHeaderColumn>
                <TableHeaderColumn dataField="createdAt" hidden />
                <TableHeaderColumn dataField="_id" dataFormat={this.actionsFormatter} width="200" isKey>Actions</TableHeaderColumn>
              </BootstrapTable>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

CustomerOrdersComponent.propTypes = {
  customer: React.PropTypes.object,
  orders: React.PropTypes.array
}

CustomerOrdersComponent.defaultProps = {

}

CustomerOrdersComponent.contextTypes = {
  router: React.PropTypes.object
}

export default CustomerOrdersComponent
