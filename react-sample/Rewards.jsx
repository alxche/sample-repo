/* global Bert */
import React from 'react'
import { Button } from 'react-bootstrap'

import RewardTable from './RewardTable'
import RewardCreate from './RewardCreate'

class RewardsComponent extends React.Component {

  constructor(props) {
    super(props)

    this.state = {
      created: false
    }
  }

  render() {
    const { rewards } = this.props

    return (
      <div>
        <div className="border-bottom white-bg page-heading">
          <div className="container-fluid">
            <h2>Rewards</h2>
            <span>Sub.</span>
          </div>
        </div>
        <div className="ibox float-e-margins">
          <div className="ibox-content">
            {!this.state.created ?
              <Button bsStyle="primary" onClick={() => this.setState({ created: true })}>
                Create Reward
              </Button> :
              <RewardCreate close={() => this.setState({ created: false })} />
            }
            <RewardTable rewards={rewards} />
          </div>
        </div>
      </div>
    )
  }
}

RewardsComponent.propTypes = {
  rewards: React.PropTypes.array
}

RewardsComponent.defaultProps = {

}

export default RewardsComponent
