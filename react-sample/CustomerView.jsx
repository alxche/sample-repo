/* global Bert */
import React from 'react'
import { LinkContainer } from 'react-router-bootstrap'
import moment from 'moment'
import { Row, Col, ButtonToolbar, ButtonGroup, Button } from 'react-bootstrap'
import { Form, Field, TextField, EmailField, NumberField, TextareaField, PhoneField, SelectField, ObjectField } from '/imports/lib/srf/fields'

import { resendAccountSetupOrder, showAccountPassword } from '/imports/api/products/actions'

import CustomerMenu from './CustomerMenu.jsx'

class CustomerViewComponent extends React.Component {

  constructor(props, { router }) {
    super(props)

    this.router = router

    this.state = {
    }
  }

  render() {
    const { customer } = this.props

    return (
      <div>
        <div className="border-bottom white-bg p-20">
          <div className="container-fluid">
            <h1 className="m-none">Customer</h1>
          </div>
        </div>
        <div className="ibox float-e-margins">
          <div className="ibox-content">
            <div className="m-t-20">
              <CustomerMenu customer={customer} />
              <Row>
                <Col md={4}>
                  <h3>Date account created</h3>
                  <p>{moment(customer.createdAt).format('MM/DD/YYYY h:mm a')}</p>
                </Col>
                <Col md={4}>
                  <h3>Date Last Login</h3>
                  <p>{moment(customer.lastLoginAt).format('MM/DD/YYYY h:mm a')}</p>
                </Col>
              </Row>
              <hr />
              <Row>
                <Col md={4}>
                  <h3>Customer Contact</h3>
                  <address>
                    <strong>{customer.name()}</strong><br />
                    <a href={`mailto:${customer.emailAddress()}`}>{customer.emailAddress()}</a><br />
                    <abbr title="Phone">P:</abbr> {customer.phone()}
                  </address>
                </Col>
                <Col md={4}>
                  <h3>Billed To</h3>
                  <address>
                    <strong>{customer.name()}</strong><br />
                    {customer.profile.billingAddress &&
                      <div>
                        {customer.profile.billingAddress.line1}<br />
                        {customer.billingAddressFormatted()}<br />
                      </div>
                    }
                  </address>
                </Col>
                <Col md={4}>
                  <h3>Shipped To</h3>
                  <address>
                    <strong>{customer.name()}</strong><br />
                    {customer.profile.shippingAddress &&
                      <div>
                        {customer.profile.shippingAddress.line1}<br />
                        {customer.shippedAddressFormatted()}<br />
                      </div>
                    }
                  </address>
                </Col>
              </Row>
              <ButtonToolbar>
                <ButtonGroup>
                  <Button bsStyle="primary" onClick={() => showAccountPassword(customer._id)}>Show Password</Button>
                </ButtonGroup>
                <ButtonGroup>
                  <Button bsStyle="primary" onClick={() => resendAccountSetupOrder(customer._id)}>Resend Account Access</Button>
                </ButtonGroup>
                <ButtonGroup>
                  <LinkContainer to={this.router.paths.customerOrders(customer._id)}>
                    <Button bsStyle="primary">View Purchases</Button>
                  </LinkContainer>
                </ButtonGroup>
              </ButtonToolbar>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

CustomerViewComponent.propTypes = {
  customer: React.PropTypes.object
}

CustomerViewComponent.defaultProps = {

}

CustomerViewComponent.contextTypes = {
  router: React.PropTypes.object
}

export default CustomerViewComponent
