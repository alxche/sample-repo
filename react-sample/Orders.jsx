/* global Bert */
import React from 'react'
import { Link } from 'react-router'
import { LinkContainer } from 'react-router-bootstrap'
import { Nav, NavItem, ButtonToolbar, ButtonGroup, Button } from 'react-bootstrap'
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table'

import * as actions from '/imports/api/products/actions'

class OrdersComponent extends React.Component {

  constructor(props, { router }) {
    super(props)

    this.actionsCell = this.actionsCell.bind(this)

    this.router = router

    this.state = {
      activeKey: 'active'
    }
  }

  actionsCell(orderId) {
    return (
      <div>
        <Link to={this.router.paths.orderEdit(orderId)} className="btn btn-default">
          <i className="fa fa-edit m-r-5" />
          Edit
        </Link>
        <Link to={this.router.paths.orderView(orderId)} className="btn btn-default">
          <i className="fa fa-eye m-r-5" />
          Invoice
        </Link>
        <a href="#" className="btn btn-danger" onClick={() => actions.cancelOrder(orderId)} title="Cancel & Revoke Access">
          <i className="fa fa-trash" />
          Cancel
        </a>
      </div>
    )
  }

  render() {
    const { orders } = this.props
    const { activeKey } = this.state
    const options = {
      defaultSortName: 'createdAt',
      defaultSortOrder: 'desc',
      sizePerPage: 25,
      noDataText: 'There are no digital courses created yet. Please click "Create Course" to create your first product.'
    }

    let filteredOrders = []
    if (activeKey === 'refunded') {
      filteredOrders = orders.filter(order => order.status === 'refunded')
    } else if (activeKey === 'deleted') {
      filteredOrders = orders.filter(order => order.status === 'deleted')
    } else {
      filteredOrders = orders.filter(order => order.status !== 'refunded' && order.status !== 'deleted')
    }

    return (
      <div>
        <div className="border-bottom white-bg p-20">
          <div className="container-fluid">
            <ButtonToolbar className="pull-right">
              <ButtonGroup>
                <LinkContainer to={this.router.paths.orderCreate()}>
                  <Button bsStyle="default">
                    <i className="fa fa-plus m-r-5" />
                    Create Customer Order
                  </Button>
                </LinkContainer>
              </ButtonGroup>
            </ButtonToolbar>
            <h1 className="m-none">Orders</h1>
          </div>
        </div>
        <div className="ibox float-e-margins">
          <div className="ibox-content">
            <div className="m-t-20">
              <p className="alert alert-info">This is where you can manage the orders for any of your products. You can add people to products here, manage refunds and process payments for orders as well.</p>
              <Nav
                className="nav-secondary"
                bsStyle="pills"
                activeKey={activeKey}
                style={{ top: 15 }}
                onSelect={selectedKey => this.setState({ activeKey: selectedKey })}
              >
                <NavItem eventKey="active">Active</NavItem>
                <NavItem eventKey="refunded">Refunded</NavItem>
                <NavItem eventKey="deleted">Revoked</NavItem>
              </Nav>
              <BootstrapTable data={filteredOrders} options={options} striped hover pagination search exportCSV>
                <TableHeaderColumn dataField="purchasedAt" width="150" dataSort>Order Date</TableHeaderColumn>
                <TableHeaderColumn dataField="email" width="250" dataSort>Customer Email</TableHeaderColumn>
                <TableHeaderColumn dataField="name" width="200" dataSort>Customer Name</TableHeaderColumn>
                <TableHeaderColumn
                  dataField="productName"
                  width="200"
                  dataFormat={(name, order) => (
                    <Link to={this.router.paths.productDetails(order.productId)}>{name}</Link>
                  )}
                  dataSort
                >
                  Product
                </TableHeaderColumn>
                <TableHeaderColumn dataField="statusFormatted" width="120" dataSort>Order Status</TableHeaderColumn>
                <TableHeaderColumn dataField="totalFormatted" width="100" dataSort>Order Total</TableHeaderColumn>
                <TableHeaderColumn dataField="paymentSource" width="120" dataSort>Payment Source</TableHeaderColumn>
                <TableHeaderColumn dataField="createdAt" hidden />
                <TableHeaderColumn dataField="_id" dataFormat={this.actionsCell} width="200" isKey>Actions</TableHeaderColumn>
              </BootstrapTable>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

OrdersComponent.propTypes = {
  orders: React.PropTypes.array
}

OrdersComponent.defaultProps = {

}

OrdersComponent.contextTypes = {
  router: React.PropTypes.object
}

export default OrdersComponent
