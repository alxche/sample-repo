import React from 'react'
import { Nav, NavItem } from 'react-bootstrap'
import { LinkContainer } from 'react-router-bootstrap'

class ProductMenuComponent extends React.Component {

  constructor(props) {
    super(props)

    this.state = {
    }
  }

  render() {
    const { router } = this.context
    const { reward } = this.props
    const rewardId = reward._id

    return (
      <Nav bsStyle="pills" className="nav-secondary" activeHref={router.location.pathname}>
        <LinkContainer to={router.paths.rewardEdit(rewardId)}>
          <NavItem>Customize</NavItem>
        </LinkContainer>
        <LinkContainer to={router.paths.rewardRules(rewardId)}>
          <NavItem>Configure</NavItem>
        </LinkContainer>
      </Nav>
    )
  }
}

ProductMenuComponent.propTypes = {
  reward: React.PropTypes.object
}

ProductMenuComponent.defaultProps = {

}

ProductMenuComponent.contextTypes = {
  router: React.PropTypes.object
}

export default ProductMenuComponent
