/* global Bert */
import React from 'react'
import { Link } from 'react-router'
import { Button } from 'react-bootstrap'
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table'
import { Form, Field } from 'simple-react-form'
import { TextField, SelectField } from 'simple-react-form-bootstrap'

import { ProductLessons } from '/imports/api/products'

import { removeLesson } from '/imports/api/products/actions'

import ProductTopBanner from './ProductTopBanner'
import ProductMenu from './ProductMenu'

class ProductLessonsComponent extends React.Component {

  constructor(props, { router }) {
    super(props)

    this.actionsFormatter = this.actionsFormatter.bind(this)
    this.packagesFormatter = this.packagesFormatter.bind(this)

    this.router = router

    this.state = {
      newLesson: false,
      lesson: {
        productId: props.product._id
      }
    }
  }

  onSuccess() {
    Bert.alert('Lesson saved', 'success', 'fixed-top', 'fa-smile-o')
    this.setState({ newLesson: false })
  }

  actionsFormatter(lessonId, lesson) {
    const { product } = this.props

    return (
      <div>
        <Link to={this.router.paths.productLessonEdit(product._id, lessonId)} className="btn btn-default">
          <i className="fa fa-edit" />
        </Link>
        <a href="#" className="btn btn-default" onClick={() => removeLesson(lessonId)}>
          <i className="fa fa-remove" />
        </a>
      </div>
    )
  }

  packagesFormatter(packageIds, lesson) {
    return (
      <div>
        {lesson.productPackages()
          .map(pkg =>
            <span className="label label-success m-r-5" key={pkg.id}>{pkg.name}</span>
          )
        }
      </div>
    )
  }

  render() {
    const { product, lessons } = this.props
    const { newLesson, lesson } = this.state
    const options = {
      defaultSortName: 'createdAt',
      defaultSortOrder: 'desc',
      sizePerPage: 25,
      noDataText: 'There are no digital courses created yet. Please click "Create Course" to create your first product.'
    }

    return (
      <div>
        <div className="border-bottom white-bg p-t-10 p-b-10">
          <div className="container-fluid">
            <ProductTopBanner product={product} />
          </div>
        </div>
        <div className="ibox float-e-margins">
          <div className="ibox-content">
            <ProductMenu product={product} />
            <p className="space-10">This allows you to create the individual lessons within your course. You can have as many lessons as you want. You will place them in order within the Lesson Structure section. This allows you to create a lesson plan.</p>
            <div className="space-20">
              {!newLesson &&
                <Button bsStyle="primary" onClick={() => this.setState({ newLesson: true })}>
                  Create Lesson
                </Button>
              }
              {newLesson &&
                <div className="ui segment">
                  <Form doc={lesson} collection={ProductLessons} type="insert" onSuccess={() => this.onSuccess()}>
                    <Field fieldName="title" type={TextField} />
                    <Button bsStyle="primary" type="submit">Create</Button>
                    <Button bsStyle="default" type="button" onClick={() => this.setState({ newLesson: false })}>Cancel</Button>
                  </Form>
                </div>
              }
            </div>
            <div className="m-t-20">
              <BootstrapTable data={lessons} options={options} striped hover pagination search>
                <TableHeaderColumn dataField="title" dataSort>Title</TableHeaderColumn>
                <TableHeaderColumn dataField="packageIds" dataFormat={this.packagesFormatter} width="200" dataSort>Package</TableHeaderColumn>
                <TableHeaderColumn dataField="createdAt" hidden />
                <TableHeaderColumn dataField="_id" dataFormat={this.actionsFormatter} isKey>Actions</TableHeaderColumn>
              </BootstrapTable>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

ProductLessonsComponent.propTypes = {
  product: React.PropTypes.object,
  lessons: React.PropTypes.array
}

ProductLessonsComponent.defaultProps = {

}

ProductLessonsComponent.contextTypes = {
  router: React.PropTypes.object
}

export default ProductLessonsComponent
