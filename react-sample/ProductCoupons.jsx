/* global Bert */
import React from 'react'
import { Button } from 'react-bootstrap'
import { Form, Field } from 'simple-react-form'
import { TextField, SelectField, RadioField } from 'simple-react-form-bootstrap'

import { Products } from '/imports/api/products'

import ProductTopBanner from './ProductTopBanner'
import ProductMenu from './ProductMenu'

class ProductCouponsComponent extends React.Component {

  constructor(props) {
    super(props)

    this.state = {
    }
  }

  render() {
    const { product, coupons } = this.props

    return (
      <div>
        <div className="border-bottom white-bg p-t-10 p-b-10">
          <div className="container-fluid">
            <ProductTopBanner product={product} />
          </div>
        </div>
        <div className="ibox float-e-margins">
          <div className="ibox-content">
            <ProductMenu product={product} />
            <div className="m-t-20">
              <Form doc={product} collection={Products} type="update" onSuccess={() => this.onSuccess()}>
                <Button bsStyle="primary" type="submit">Save Product</Button>
              </Form>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

ProductCouponsComponent.propTypes = {
  product: React.PropTypes.object,
  coupons: React.PropTypes.array
}

ProductCouponsComponent.defaultProps = {

}

export default ProductCouponsComponent
