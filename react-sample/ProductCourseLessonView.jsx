import React from 'react'

import ProductLessonView from './ProductLessonView.jsx'
import ProductCourseContent from './ProductCourseContent.jsx'

class ProductCourseLessonViewComponent extends React.Component {

  constructor(props) {
    super(props)

    this.state = {
    }
  }

  render() {
    const { product, order, lesson, lessons } = this.props
    const author = product.author()

    return (
      <div className="fh-breadcrumb">
        {/*<div className="fh-column">
          <div className="full-height-scroll">
            <ProductCourseContent product={product} lessons={lessons} lesson={lesson} />
          </div>
        </div>
        <div className="full-height">
          <div className="full-height-scroll white-bg border-left">
            <div className="element-detail-box">
              <ProductLessonView lesson={lesson} order={order} />
            </div>
          </div>
        </div>*/}
        <div className="row">
          <div className="col-lg-3">
            <ProductCourseContent product={product} lessons={lessons} lesson={lesson} />
          </div>
          <div className="col-lg-9">
            <ProductLessonView lesson={lesson} order={order} />
          </div>
        </div>
        {/*<div className="footer p-tb-20 p-lr-10 text-center" style={{ position: 'relative' }}>
          <p className="text-muted text-sm">
            © 2016 {author && author.companyName()}. All right reserved.
          </p>
        </div>*/}
      </div>
    )
  }
}

ProductCourseLessonViewComponent.propTypes = {
  product: React.PropTypes.object,
  order: React.PropTypes.object,
  lesson: React.PropTypes.object,
  lessons: React.PropTypes.array
}

ProductCourseLessonViewComponent.defaultProps = {

}

export default ProductCourseLessonViewComponent
