import { Memberships } from 'meteor/rgnevashev:memberships'

import React from 'react'
import { Row, Col, Panel, Button } from 'react-bootstrap'
import { Form, Field, TextField, EmailField, NumberField, TextareaField, PhoneField, SelectField, ObjectField } from '/imports/lib/srf/fields'
import InputMaskField from '/imports/lib/fields/InputMask'

import {
  countriesOptions,
  statesOptions
} from '/imports/api/common/options'

import { Products } from '/imports/api/products'

import { createOrder } from '/imports/api/products/actions'

class OrderCreateComponent extends React.Component {

  constructor(props, { router }) {
    super(props)

    this.productOptions = this.productOptions.bind(this)
    this.selectedProduct = this.selectedProduct.bind(this)
    this.selectedCustomer = this.selectedCustomer.bind(this)
    this.paymentOptions = this.paymentOptions.bind(this)
    this.onChange = this.onChange.bind(this)

    this.router = router

    this.state = {
      order: {
        shippingAddress: {
          country: 'us'
        },
        billingAddress: {
          country: 'us'
        },
        quantity: 1
      },
      submitting: false,
      leadId: null
    }
  }

  onChange(changes) {
    this.setState({ order: changes })
  }

  onSubmit(order) {
    this.setState({ submitting: true })
    createOrder(order, (err) => {
      this.setState({ submitting: false })
      if (!err) {
        this.router.push('/orders')
      }
    })
  }

  customerTypeOptions() {
    const options = [
      { label: 'Choose Customer Type', value: '' },
      { label: 'New Customer', value: 'new' },
      { label: 'From Customers', value: 'customer' },
      { label: 'From Leads', value: 'lead' }
    ]
    if (Memberships.hasRole('admin')) {
      options.push({ label: 'From Clients', value: 'client' })
    }
    return options
  }

  productOptions() {
    const { products } = this.props
    const options = (products.map(product => (
      { label: product.name, value: product._id }
    ))) || []
    options.unshift({ label: 'Choose Product', value: '' })
    return options
  }

  paymentOptions() {
    const customer = this.selectedCustomer()
    const options = [
      { label: 'Without Payment', value: 'no' },
      { label: 'New Payment', value: 'new' }
    ]
    if (customer && customer.card) {
      options.unshift({ label: `${customer.card.brand} - ${customer.card.last4}`, value: customer.card.id })
    }
    options.unshift({ label: 'Choose Payment Type', value: '' })
    return options
  }

  selectedProduct() {
    const { order } = this.state
    return Products.findOne(order.productId)
  }

  selectedCustomer() {
    const { order } = this.state
    return Meteor.users.findOne(order.userId)
  }

  render() {
    const { order, submitting } = this.state
    const product = this.selectedProduct()

    return (
      <div>
        <div className="border-bottom white-bg p-20">
          <div className="container-fluid">
            <h1 className="m-none">Add Order</h1>
          </div>
        </div>
        <div className="ibox float-e-margins">
          <div className="ibox-content">
            <div className="m-t-20">
              <Form
                doc={order}
                type="function"
                onSubmit={doc => this.onSubmit(doc)}
                onChange={changes => this.onChange(changes)}
              >
                <Panel header="Product">
                  <Field fieldName="productId" type={SelectField} options={this.productOptions()} placeholder="Product" />
                  {product && product.isPackages() &&
                    <Field fieldName="packageId" type={SelectField} options={product.packageOptions()} placeholder="Package Level" />
                  }
                  {product && product.isRecurring() &&
                    <Field fieldName="planId" type={SelectField} options={product.planOptions()} placeholder="Plan" />
                  }
                  <Row>
                    <Col md={6}>
                      {product && product.isShipping() &&
                        <div>
                          <Field fieldName="shippingAddress" type={ObjectField} label="Shipping Address">
                            <Field fieldName="line1" type={TextField} placeholder="Address 1" />
                            <Field fieldName="line2" type={TextField} placeholder="Address 2" />
                            <div className="row">
                              <div className="col-sm-4">
                                <Field fieldName="zip" type={TextField} placeholder="Zip" />
                              </div>
                              <div className="col-sm-4">
                                <Field fieldName="city" type={TextField} placeholder="City" />
                              </div>
                              <div className="col-sm-4">
                                {(order.shippingAddress && order.shippingAddress.country === 'us') ?
                                  <Field fieldName="state" type={SelectField} options={statesOptions} placeholder="State" /> :
                                  <Field fieldName="county" type={TextField} placeholder="County" />
                                }
                              </div>
                            </div>
                            <Field fieldName="country" type={SelectField} options={countriesOptions} placeholder="Country" />
                          </Field>
                        </div>
                      }
                    </Col>
                    <Col md={6}>
                      {product &&
                        <div>
                          <Field fieldName="billingAddress" type={ObjectField} label="Billing Address">
                            <Field fieldName="line1" type={TextField} placeholder="Address 1" />
                            <Field fieldName="line2" type={TextField} placeholder="Address 2" />
                            <div className="row">
                              <div className="col-sm-4">
                                <Field fieldName="zip" type={TextField} placeholder="Zip" />
                              </div>
                              <div className="col-sm-4">
                                <Field fieldName="city" type={TextField} placeholder="City" />
                              </div>
                              <div className="col-sm-4">
                                {(order.billingAddress && order.billingAddress.country === 'us') ?
                                  <Field fieldName="state" type={SelectField} options={statesOptions} placeholder="State" /> :
                                  <Field fieldName="county" type={TextField} placeholder="County" />
                                }
                              </div>
                            </div>
                            <Field fieldName="country" type={SelectField} options={countriesOptions} placeholder="Country" />
                          </Field>
                        </div>
                      }
                    </Col>
                  </Row>
                </Panel>
                <Panel header="Customer">
                  <Field fieldName="customerType" type={SelectField} options={this.customerTypeOptions()} placeholder="Customer Type" selectable />
                  {order.customerType === 'new' &&
                    <div>
                      <div className="row">
                        <div className="col-sm-6">
                          <Field fieldName="firstName" type={TextField} placeholder="First Name" />
                        </div>
                        <div className="col-sm-6">
                          <Field fieldName="lastName" type={TextField} placeholder="Last Name" />
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-sm-6">
                          <Field fieldName="email" type={EmailField} placeholder="Email Address" />
                        </div>
                        <div className="col-sm-6">
                          <Field fieldName="phone" type={PhoneField} placeholder="Phone" />
                        </div>
                      </div>
                    </div>
                  }
                  {order.customerType === 'lead' &&
                    <div className="form-group">
                      <Field fieldName="leadId" type={SelectField} placeholder="Search Leads, Start type..." method="searchLeads" labelKey="label" valueKey="value" async />
                    </div>
                  }
                  {order.customerType === 'customer' &&
                    <div className="form-group">
                      <Field fieldName="customerId" type={SelectField} placeholder="Search Customers, Start type..." method="searchProductCustomers" labelKey="label" valueKey="value" async />
                    </div>
                  }
                  {order.customerType === 'client' &&
                    <div className="form-group">
                      <Field fieldName="clientId" type={SelectField} placeholder="Search Clients, Start type..." method="searchClients" labelKey="label" valueKey="value" async />
                    </div>
                  }
                </Panel>
                <Panel header="Payment Method">
                  <Field fieldName="paymentType" type={SelectField} options={this.paymentOptions()} placeholder="Payment Method" />
                  {order.paymentType === 'new' &&
                    <div>
                      <div className="m-b-10">
                        <img className="cl-credit-cards" src="/img/cards.png" style={{ maxWidth: 300 }} />
                      </div>
                      <Field fieldName="source.number" type={TextField} placeholder="1234 5678 9012 3456" />
                      <div className="row">
                        <div className="col-sm-8">
                          <Field fieldName="source.exp" type={InputMaskField} placeholder="MM/YY" mask="99/99" className="form-control" />
                        </div>
                        <div className="col-sm-4">
                          <Field fieldName="source.cvc" type={TextField} placeholder="132" />
                        </div>
                      </div>
                    </div>
                  }
                  <Field fieldName="coupon" type={TextField} placeholder="Coupon Code" />
                  <Field fieldName="notes" type={TextareaField} placeholder="Notes" />
                </Panel>
                <Panel header="Order Summary">
                  {product && product.isPhysical() && order.paymentType !== 'no' &&
                    <div className="row">
                      <div className="col-sm-4">
                        <Field fieldName="quantity" type={NumberField} label="Quantity" placeholder="Quantity" forceLabel />
                      </div>
                      <div className="col-sm-4">
                        <Field fieldName="subtotal" type={NumberField} label="Subtotal" placeholder={product.subtotal(order).toString()} step=".01" forceLabel />
                      </div>
                      <div className="col-sm-4">
                        {product.isShipping() &&
                          <Field fieldName="shippingCost" type={NumberField} label="Shipping Cost" placeholder={product.shippingCost(order).toString()} step=".01" forceLabel />
                        }
                      </div>
                    </div>
                  }
                  {product && order.paymentType !== 'no' &&
                    <div>
                      {product.isRecurring() ?
                        <div>
                          <div className="row">
                            <div className="col-md-6">
                              <Field
                                fieldName="trialDays"
                                type={NumberField}
                                label="Trial in Days"
                                placeholder={(product.planFormatted(order.planId) && product.planFormatted(order.planId).trialDays) || '0'}
                                forceLabel
                              />
                            </div>
                            <div className="col-md-6">
                              <Field
                                fieldName="trialAmount"
                                type={NumberField}
                                label="Trial Amount"
                                placeholder={(product.planFormatted(order.planId) && product.planFormatted(order.planId).trialAmount) || '0'}
                                step=".01"
                                forceLabel
                              />
                            </div>
                          </div>
                          <Field fieldName="total" type={NumberField} label="Plan Amount" placeholder={product.total(order).toString()} step=".01" forceLabel />
                        </div> :
                        <Field fieldName="total" type={NumberField} label="Total" placeholder={product.total(order).toString()} step=".01" forceLabel />
                      }
                    </div>
                  }
                  {order.paymentType === 'no' &&
                    <div>Free</div>
                  }
                </Panel>
                <Button bsStyle="primary" type="submit" disabled={submitting}>
                  {submitting ? 'Submitting...' : 'Create Order'}
                </Button>
              </Form>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

OrderCreateComponent.propTypes = {
  products: React.PropTypes.array,
  customers: React.PropTypes.array,
  leads: React.PropTypes.array
}

OrderCreateComponent.defaultProps = {

}

OrderCreateComponent.contextTypes = {
  router: React.PropTypes.object
}

export default OrderCreateComponent
