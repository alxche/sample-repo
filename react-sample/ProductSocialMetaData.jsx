/* global Bert */
import React from 'react'
import { Button } from 'react-bootstrap'
import { Form, Field } from 'simple-react-form'
import { TextField, TextareaField } from 'simple-react-form-bootstrap'
import DropzoneField from '/imports/lib/fields/Dropzone'

import { Products } from '/imports/api/products'

import ProductTopBanner from './ProductTopBanner'
import ProductMenu from './ProductMenu'

class ProductSocialMetaDataComponent extends React.Component {

  constructor(props) {
    super(props)

    this.state = {
    }
  }

  onSuccess() {
    Bert.alert('Meta Data saved', 'success', 'fixed-top', 'fa-smile-o')
  }

  render() {
    const { product } = this.props

    return (
      <div>
        <div className="border-bottom white-bg p-t-10 p-b-10">
          <div className="container-fluid">
            <ProductTopBanner product={product} />
          </div>
        </div>
        <div className="ibox float-e-margins">
          <div className="ibox-content">
            <ProductMenu product={product} />
            <div className="m-t-20">
              <Form doc={product} collection={Products} type="update" onSuccess={() => this.onSuccess()}>
                <Field fieldName="socialMetaData.title" type={TextField} />
                <Field fieldName="socialMetaData.description" type={TextareaField} />
                <Field fieldName="socialMetaData.image" type={DropzoneField} />
                <Field fieldName="socialMetaData.keywords" type={TextField} />
                <Button bsStyle="primary" type="submit">Save Product</Button>
              </Form>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

ProductSocialMetaDataComponent.propTypes = {
  product: React.PropTypes.object
}

ProductSocialMetaDataComponent.defaultProps = {

}

export default ProductSocialMetaDataComponent
