import React from 'react'
import { Form, Field } from 'simple-react-form'
import { TextField } from 'simple-react-form-bootstrap'
import FroalaEditor from '/imports/lib/fields/FroalaEditor'
import Toggle from '/imports/lib/fields/Toggle'

import { Rewards } from '/imports/api/products'

import RewardMenu from './RewardMenu'

class RewardEditComponent extends React.Component {

  constructor(props) {
    super(props)

    this.state = {
      reward: props.reward
    }
  }

  onSuccess() {
    Bert.alert('Reward Details saved', 'success', 'fixed-top', 'fa-smile-o')
  }

  render() {
    const { reward } = this.state

    return (
      <div>
        <div className="border-bottom white-bg p-t-10 p-b-10">
          <div className="container-fluid">
            <h2>Reward Edit</h2>
            <span>Sub.</span>
          </div>
        </div>
        <div className="ibox float-e-margins">
          <div className="ibox-content">
            <RewardMenu reward={reward} />
            <div className="m-t-20">
              <Form
                doc={reward}
                collection={Rewards}
                type="update"
                onChange={changes => this.setState({ reward: changes })}
                onSuccess={() => this.onSuccess()}
              >
                <Field fieldName="name" type={TextField} label="Name" />
                <Field fieldName="active" type={Toggle} label="Active" />
                <Field fieldName="headline" type={TextField} label="Headline" />
                <Field fieldName="content" type={FroalaEditor} label="Content" />
                <button className="btn btn-primary" type="submit">Save</button>
              </Form>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

RewardEditComponent.propTypes = {
  reward: React.PropTypes.object
}

RewardEditComponent.defaultProps = {

}

export default RewardEditComponent

/*
                <Field fieldName="type" type={SelectField} options={this.rewardTypeOptions()} />
                {reward.type === 'QL-Trial' &&
                  <Field fieldName="params" type={ObjectField} showLabel={false}>
                    <Field fieldName="days" type={NumberField} label="Trial Days" />
                  </Field>
                }
*/
