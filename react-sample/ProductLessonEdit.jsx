/* global Bert */
import React from 'react'
import { Button } from 'react-bootstrap'
import { translate } from 'react-i18next'
import { Form, Field } from 'simple-react-form'
import { TextField, DatetimeField, NumberField, TextareaField, SelectField, ArrayField } from 'simple-react-form-bootstrap'
import ComboboxField from '/imports/lib/fields/Select'
import Toggle from '/imports/lib/fields/Toggle'

import { ProductLessons } from '/imports/api/products'

import { withSectionContent, withSort } from '/imports/ui/decorators'

import ProductLessonView from './ProductLessonView'
import ProductTopBanner from './ProductTopBanner'
import ProductMenu from './ProductMenu'

@withSectionContent()
@withSort
class ProductLessonEditComponent extends React.Component {

  static contextTypes = {
    router: React.PropTypes.object
  }

  constructor(props, { router }) {
    super(props)

    this.router = router

    this.state = {
      lesson: props.lesson
    }
  }

  componentWillReceiveProps(nextProps) {
    this.setState({ lesson: nextProps.lesson })
  }

  onSort(event) {
    const { lesson } = this.state
    lesson.sections.move(event.oldIndex, event.newIndex)
    this.sortable.sort(this.sortable.toArray().move(event.newIndex, event.oldIndex))
    this.setState({ lesson })
    this.refs.pl.setState({ doc: lesson }) // fix bug sort
  }

  onSuccess() {
    Bert.alert('Lesson saved', 'success', 'fixed-top', 'fa-smile-o')
  }

  render() {
    const { product } = this.props
    const { lesson } = this.state

    return (
      <div>
        <div className="border-bottom white-bg p-t-10 p-b-10">
          <div className="container-fluid">
            <ProductTopBanner product={product} />
          </div>
        </div>
        <div className="ibox float-e-margins">
          <div className="ibox-content">
            <ProductMenu product={product} />
            <div className="m-t-20">
              <div className="row">
                <div className="col-sm-12 col-lg-6">
                  <Form
                    doc={lesson}
                    collection={ProductLessons}
                    type="update"
                    onSuccess={() => this.onSuccess()}
                    onChange={changes => this.setState({ lesson: changes })}
                    className="ui segment"
                    ref="pl"
                  >
                    <Field fieldName="title" type={TextField} />
                    {product.isPackages() &&
                      <Field fieldName="packageIds" type={ComboboxField} options={product.packageOptions()} label="Package Levels" multi />
                    }
                    {product.isRecurring() &&
                      <Field fieldName="planIds" type={ComboboxField} options={product.planOptions()} label="Plans" multi />
                    }
                    <Field fieldName="dripRelease" type={Toggle} label="Drip Release" />
                    {lesson.dripRelease &&
                      <div className="row">
                        <div className="col-md-5">
                          <Field fieldName="daysFromPurchase" type={NumberField} label="Days From Purchase" />
                        </div>
                        <div className="col-md-2 text-center m-t-25">
                          - or -
                        </div>
                        <div className="col-md-5">
                          <Field fieldName="dripReleaseAt" type={DatetimeField} label="Drip Release Date" timeFormat={false} closeOnSelect />
                        </div>
                      </div>
                    }
                    <Field fieldName="sections" type={ArrayField} label="Sections" renderItem={this.renderSection} childrenClassName="row" parentClassName="sortable-container" />
                    <Button bsStyle="primary" type="submit">Save Changes</Button>
                    <Button onClick={() => this.router.push(this.router.paths.productLessons(product._id))}>Return to Lessons</Button>
                  </Form>
                </div>
                <div className="col-sm-12 col-lg-6">
                  <div className="text-center">
                    <Button
                      onClick={() => this.router.push(this.router.paths.productLessonEdit(product._id, product.prevLesson(lesson._id)))}
                      disabled={!product.prevLesson(lesson._id)}
                    >
                      Prev Lesson
                    </Button>
                    <Button bsStyle="primary" onClick={() => this.refs.pl.submit()}>Save Lesson</Button>
                    <Button
                      onClick={() => this.router.push(this.router.paths.productLessonEdit(product._id, product.nextLesson(lesson._id)))}
                      disabled={!product.nextLesson(lesson._id)}
                    >
                      Next Lesson
                    </Button>
                    <Button href={this.router.paths.productCourseLessonView(product._id, lesson._id)} target="_blank">
                      Preview Lesson
                    </Button>
                  </div>
                  <div className="preview-panel bg-white">
                    <ProductLessonView lesson={lesson} />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

ProductLessonEditComponent.propTypes = {
  product: React.PropTypes.object,
  lesson: React.PropTypes.object
}

ProductLessonEditComponent.defaultProps = {

}

export default translate('landing', { wait: true })(ProductLessonEditComponent)
