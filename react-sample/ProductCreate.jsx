/* global Bert */
import React from 'react'
import { Button } from 'react-bootstrap'
import { Form, Field } from 'simple-react-form'
import { TextField, SelectField, TextareaField } from 'simple-react-form-bootstrap'

import { Products } from '/imports/api/products'

class ProductCreateComponent extends React.Component {

  constructor(props, { router }) {
    super(props)

    this.router = router

    this.state = {
      product: {
        currency: 'usd'
      }
    }
  }

  onSuccess() {
    Bert.alert('Success', 'success')
    this.router.push('/products')
  }

  render() {
    const { product } = this.state

    return (
      <div>
        <div className="border-bottom white-bg page-heading">
          <div className="container-fluid">
            <h2>New Product</h2>
            <span></span>
          </div>
        </div>
        <div className="ibox float-e-margins">
          <div className="ibox-content">
            <Form doc={product} collection={Products} type="insert" onSuccess={() => this.onSuccess()}>
              <Field fieldName="name" type={TextField} />
              {/*<div className="row">
                <div className="col-sm-6">
                  <Field fieldName="price" type={TextField} />
                </div>
                <div className="col-sm-6">
                  <Field fieldName="currency" type={SelectField} />
                </div>
              </div>*/}
              <Field fieldName="description" type={TextareaField} />
              <Button bsStyle="primary" type="submit">Create Product</Button>
            </Form>
          </div>
        </div>
      </div>
    )
  }
}

ProductCreateComponent.propTypes = {

}

ProductCreateComponent.defaultProps = {

}

ProductCreateComponent.contextTypes = {
  router: React.PropTypes.object
}

export default ProductCreateComponent
