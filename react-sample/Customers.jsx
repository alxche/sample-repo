import React from 'react'
import { LinkContainer } from 'react-router-bootstrap'
import { ButtonGroup, Button } from 'react-bootstrap'
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table'

class CustomersComponent extends React.Component {

  constructor(props, { router }) {
    super(props)

    this.actionsFormatter = this.actionsFormatter.bind(this)

    this.router = router

    this.state = {
    }
  }

  actionsFormatter(customerId) {
    return (
      <ButtonGroup>
        <LinkContainer to={this.router.paths.customerView(customerId)}>
          <Button>
            <i className="fa fa-eye fa-fw" />
          </Button>
        </LinkContainer>
      </ButtonGroup>
    )
  }

  render() {
    const { customers } = this.props
    const options = {
      defaultSortName: 'createdAt',
      defaultSortOrder: 'desc',
      sizePerPage: 25,
      noDataText: 'There are no customers created yet.'
    }

    return (
      <div>
        <div className="border-bottom white-bg p-20">
          <div className="container-fluid">
            <h1 className="m-none">Customers</h1>
          </div>
        </div>
        <div className="ibox float-e-margins">
          <div className="ibox-content">
            <div className="m-t-20">
              <p className="alert alert-info">This is where you can manage the customers for any of your products.</p>
              <BootstrapTable data={customers} options={options} striped hover pagination search exportCSV>
                <TableHeaderColumn dataField="email" width="250" dataSort>Email</TableHeaderColumn>
                <TableHeaderColumn dataField="name" width="200" dataSort>Name</TableHeaderColumn>
                <TableHeaderColumn dataField="createdAt" hidden />
                <TableHeaderColumn dataField="_id" dataFormat={this.actionsFormatter} width="200" isKey>Actions</TableHeaderColumn>
              </BootstrapTable>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

CustomersComponent.propTypes = {
  customers: React.PropTypes.array
}

CustomersComponent.defaultProps = {

}

CustomersComponent.contextTypes = {
  router: React.PropTypes.object
}

export default CustomersComponent
