import React from 'react'
import { Button } from 'react-bootstrap'
import { Form, Field, TextField, SelectField, TextareaField, ColorField, CheckboxField, ArrayField } from '/imports/lib/srf/fields'
import TemplatesField from '/imports/lib/fields/Templates'
import DropzoneField from '/imports/lib/fields/Dropzone'
import FroalaField from '/imports/lib/fields/FroalaEditor'
// import EditorField from '/imports/lib/srf/fields/EditorField'
import IconpickerField from '/imports/lib/fields/Iconpicker'

import { Products } from '/imports/api/products'

import ProductTopBanner from './ProductTopBanner'
import ProductMenu from './ProductMenu'

class ProductCustomizeComponent extends React.Component {

  constructor(props) {
    super(props)

    this.state = {
      product: props.product
    }
  }

  onSuccess() {
    Bert.alert('Success', 'success')
  }

  render() {
    const { product } = this.state

    return (
      <div>
        <div className="border-bottom white-bg p-t-10 p-b-10">
          <div className="container-fluid">
            <ProductTopBanner product={product} />
          </div>
        </div>
        <div className="ibox float-e-margins">
          <div className="ibox-content">
            <ProductMenu product={product} />
            <div className="m-t-20">
              <Form doc={product} collection={Products} type="update" onSuccess={() => this.onSuccess()} onChange={changes => this.setState({ product: changes })}>
                <h3>Template Library</h3>
                <p>Choose from a library of premade templates.</p>
                <Field fieldName="template" type={TemplatesField} url={product.url()} label={false} />
                <hr />
                <h3>Template Colors</h3>
                <p>Change your template colors to match your brand.</p>
                <div className="row">
                  <div className="col-sm-2">
                    <Field fieldName="headerColor" type={ColorField} />
                  </div>
                  <div className="col-sm-2">
                    <Field fieldName="footerColor" type={ColorField} />
                  </div>
                  <div className="col-sm-2">
                    <Field fieldName="textColor" type={ColorField} />
                  </div>
                  <div className="col-sm-2">
                    <Field fieldName="backgroundColor" type={ColorField} />
                  </div>
                  <div className="col-sm-2">
                    <Field fieldName="headlineColor" type={ColorField} />
                  </div>
                  <div className="col-sm-2">
                    <Field fieldName="buttonColor" type={ColorField} />
                  </div>
                </div>
                <Field fieldName="companyLogo" type={DropzoneField} label="Marketplace Logo" />
                <hr />
                <h3>Fields</h3>
                <p>Select which form fields you would like to be active on your checkout page.</p>
                <div className="row">
                  <div className="col-sm-6">
                    <Field fieldName="enableBillingAddress" type={CheckboxField} toggle />
                    <Field
                      fieldName="enableShippingAddress"
                      type={CheckboxField}
                      value={product.isPhysical() && !product.enableBillingAddress ? true : product.enableShippingAddress}
                      disabled={product.isPhysical() && !product.enableBillingAddress}
                      toggle
                    />
                  </div>
                  <div className="col-sm-6">
                    <Field fieldName="enablePhoneNumber" type={CheckboxField} toggle />
                  </div>
                </div>
                <div className="row">
                  <div className="col-sm-6">
                    <Field fieldName="buttonText" type={SelectField} label="Checkout Button Text" />
                  </div>
                </div>
                <hr />
                <Field fieldName="bulletPointsTitle" type={TextField} />
                <div className="row">
                  <div className="col-md-4">
                    <Field fieldName="bulletPointsIcon" type={IconpickerField} hideOnSelect />
                    <Field fieldName="bulletPointsIconColor" type={ColorField} />
                    <Field fieldName="bulletPointsTextColor" type={ColorField} />
                  </div>
                  <div className="col-md-8">
                    <Field fieldName="bulletPointsImage" type={DropzoneField} />
                  </div>
                </div>
                <Field fieldName="bulletPoints" type={ArrayField} >
                  <div className="m-b-10">
                    <Field fieldName="text" type={TextField} />
                  </div>
                </Field>
                <hr />
                <Field fieldName="testimonialsTitle" type={TextField} />
                <Field fieldName="testimonials" type={ArrayField} >
                  <div className="m-b-10">
                    <Field fieldName="name" type={TextField} placeholder="Name" />
                    <Field fieldName="text" type={TextareaField} placeholder="Testimonial Text" />
                    <Field fieldName="avatar" type={DropzoneField} />
                  </div>
                </Field>
                <Field fieldName="customContent" type={FroalaField} />
                <Button bsStyle="primary" type="submit">Save Product</Button>
              </Form>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

ProductCustomizeComponent.propTypes = {
  product: React.PropTypes.object
}

ProductCustomizeComponent.defaultProps = {

}

export default ProductCustomizeComponent
