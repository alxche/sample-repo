import React from 'react'
import { LinkContainer } from 'react-router-bootstrap'
import { Nav, NavItem } from 'react-bootstrap'

class ProductMenuComponent extends React.Component {

  constructor(props) {
    super(props)

    this.state = {
    }
  }

  render() {
    const { router } = this.context
    const { product } = this.props
    const productId = product._id

    return (
      <Nav bsStyle="pills" className="nav-secondary" activeHref={router.location.pathname}>
        <LinkContainer to={router.paths.productEdit(productId)}>
          <NavItem>Type</NavItem>
        </LinkContainer>
        <LinkContainer to={router.paths.productDetails(productId)}>
          <NavItem>Details</NavItem>
        </LinkContainer>
        {product.type === 'course' &&
          <LinkContainer to={router.paths.productLessons(productId)}>
            <NavItem>Lessons</NavItem>
          </LinkContainer>
        }
        {product.type === 'course' &&
          <LinkContainer to={router.paths.productLessonStructure(productId)}>
            <NavItem>Lesson Structure</NavItem>
          </LinkContainer>
        }
        <LinkContainer to={router.paths.productCustomize(productId)}>
          <NavItem>Customize</NavItem>
        </LinkContainer>
        <LinkContainer to={router.paths.productAdvanced(productId)}>
          <NavItem>Advanced</NavItem>
        </LinkContainer>
        <LinkContainer to={router.paths.productAnalytics(productId)}>
          <NavItem>Analytics</NavItem>
        </LinkContainer>
        <LinkContainer to={router.paths.productSocialMetaData(productId)}>
          <NavItem>Social Meta Data</NavItem>
        </LinkContainer>
      </Nav>
    )
  }
}

ProductMenuComponent.propTypes = {
  product: React.PropTypes.object
}

ProductMenuComponent.defaultProps = {

}

ProductMenuComponent.contextTypes = {
  router: React.PropTypes.object
}

export default ProductMenuComponent
