/* global Bert */

import { Memberships } from 'meteor/rgnevashev:memberships'

import React from 'react'
import { Button } from 'react-bootstrap'
import { Form, Field } from 'simple-react-form'
import { TextField, SelectField } from 'simple-react-form-bootstrap'

import { Rewards } from '/imports/api/products'

class RewardCreateComponent extends React.Component {

  constructor(props, { router }) {
    super(props)

    this.router = router

    this.state = {
    }
  }

  onChange(reward) {
    this.setState({ reward })
  }

  onSuccess(rewardId) {
    Bert.alert('Reward saved', 'success', 'fixed-top', 'fa-smile-o')
    this.router.push(this.router.paths.rewardEdit(rewardId))
  }

  render() {
    return (
      <Form
        collection={Rewards}
        type="insert"
        onSuccess={rewardId => this.onSuccess(rewardId)}
      >
        <Field fieldName="name" type={TextField} />
        <Button bsStyle="primary" type="submit">Save Reward</Button>
        <Button type="button" onClick={() => this.props.close()}>Cancel</Button>
      </Form>
    )
  }
}

RewardCreateComponent.propTypes = {
  close: React.PropTypes.func
}

RewardCreateComponent.defaultProps = {

}

RewardCreateComponent.contextTypes = {
  router: React.PropTypes.object
}

export default RewardCreateComponent
