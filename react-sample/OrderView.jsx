import React from 'react'
import { Link } from 'react-router'
import { LinkContainer } from 'react-router-bootstrap'
import { Row, Col, Grid, ButtonToolbar, ButtonGroup, Button } from 'react-bootstrap'

import { refundOrder, cancelOrder, resendReceiptOrder, resendAccountSetupOrder, showAccountPassword } from '/imports/api/products/actions'

class OrderViewComponent extends React.Component {

  constructor(props, { router }) {
    super(props)

    this.router = router

    this.state = {
    }
  }

  render() {
    const { order } = this.props

    return (
      <div>
        <div className="border-bottom white-bg p-t-10 p-b-10">
          <div className="container-fluid">
            <h1>Order ID #{order._id}</h1>
          </div>
        </div>
        <div className="ibox float-e-margins">
          <div className="ibox-content">
            <div className="m-t-20">
              <Grid>
                <div>
                  <Row>
                    <Col md={4}>
                      <h3>Order Date</h3>
                      <p>{order.purchasedAt()}</p>
                    </Col>
                    <Col md={4}>
                      <h3>Order Status</h3>
                      <p>{order.status}</p>
                    </Col>
                    <Col md={4}>
                      <h3>Affiliate</h3>
                      <p>N/A</p>
                    </Col>
                  </Row>
                  <hr />
                  <Row>
                    <Col md={4}>
                      <h3>Customer Contact</h3>
                      <address>
                        <strong>{order.customerName()}</strong><br />
                        <a href={`mailto:${order.email}`}>{order.email}</a><br />
                        <abbr title="Phone">P:</abbr> {order.phone}
                      </address>
                    </Col>
                    <Col md={4}>
                      <h3>Billed To</h3>
                      <address>
                        <strong>{order.customerName()}</strong><br />
                        {order.billingAddress && order.billingAddress.line1}<br />
                        {order.billingAddressFormatted()}<br />
                      </address>
                    </Col>
                    <Col md={4}>
                      <h3>Shipped To</h3>
                      <address>
                        <strong>{order.customerName()}</strong><br />
                        {order.shippingAddress && order.shippingAddress.line1}<br />
                        {order.shippedAddressFormatted()}<br />
                      </address>
                    </Col>
                  </Row>
                  <p>{order.notes}</p>
                  <hr />
                  <h2>Order Summary</h2>
                  <table className="table">
                    <thead>
                      <tr>
                        <td width="20%"><strong>Item</strong></td>
                        <td width="10%"><strong>Price</strong></td>
                        <td width="10%"><strong>Qty</strong></td>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>
                          <Link to={this.router.paths.productDetails(order.productId)}>
                            {order.product().name}
                          </Link>
                        </td>
                        <td>
                          {order.costFormatted()}
                        </td>
                        <td>{order.quantity || 1}</td>
                      </tr>
                      {order.subtotal ?
                        <tr>
                          <td>&nbsp;</td>
                          <td>Sub Total</td>
                          <td>{order.subtotalFormatted()}</td>
                        </tr> : null
                      }
                      {order.shippingCost ?
                        <tr>
                          <td>&nbsp;</td>
                          <td>Shipping Costs</td>
                          <td>{order.shippingCostFormatted()}</td>
                        </tr> : null
                      }
                      {order.trialFormatted ?
                        <tr>
                          <td>&nbsp;</td>
                          <td>Trial</td>
                          <td>{order.trialFormatted()}</td>
                        </tr> : null
                      }
                      {order.discount ?
                        <tr>
                          <td>&nbsp;</td>
                          <td>Discount</td>
                          <td>{order.discountFormatted()}</td>
                        </tr> : null
                      }
                      <tr>
                        <td>&nbsp;</td>
                        <td>Total</td>
                        <td>{order.totalFormatted()}</td>
                      </tr>
                    </tbody>
                  </table>
                  <ButtonToolbar>
                    <LinkContainer to={this.router.paths.orderEdit(order._id)}>
                      <Button bsStyle="primary">Edit Order</Button>
                    </LinkContainer>
                    <Button bsStyle="primary" onClick={() => resendReceiptOrder(order._id)}>Resend Receipt</Button>
                    <Button bsStyle="primary" onClick={() => showAccountPassword(order.userId)}>Show Password</Button>
                    <Button bsStyle="primary" onClick={() => resendAccountSetupOrder(order.userId)}>Resend Account Access</Button>
                    <LinkContainer to={this.router.paths.customerOrders(order.userId)}>
                      <Button bsStyle="primary">View Purchases</Button>
                    </LinkContainer>
                    <ButtonGroup>
                      {(order.priceType !== 'free' && order.status !== 'refunded') &&
                        <Button
                          bsStyle="danger"
                          onClick={() => refundOrder(order._id)}
                        >
                          Refund Entire Order
                        </Button>
                      }
                      <Button
                        bsStyle="danger"
                        onClick={() =>
                          cancelOrder(order._id, (err) => {
                            if (!err) {
                              this.router.push('/orders')
                            }
                          })
                        }
                      >
                        Cancel & Revoke Access
                      </Button>
                    </ButtonGroup>
                  </ButtonToolbar>
                </div>
              </Grid>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

OrderViewComponent.propTypes = {
  order: React.PropTypes.object
}

OrderViewComponent.defaultProps = {

}

OrderViewComponent.contextTypes = {
  router: React.PropTypes.object
}

export default OrderViewComponent
