import { Memberships } from 'meteor/rgnevashev:memberships'
import React from 'react'
import { Link } from 'react-router'

import { copyProduct, removeProduct } from '/imports/api/products/actions'

class ProductsComponent extends React.Component {

  constructor(props) {
    super(props)

    this.state = {
    }
  }

  render() {
    const { router } = this.context
    const { currentUser } = this.context
    const { products } = this.props

    return (
      <div>
        <div className="border-bottom white-bg page-heading">
          <div className="container-fluid">
            <div className="pull-right m-t-20">
              <Link to={router.paths.productCreate()} className="btn btn-primary">Create Product</Link>
            </div>
            <h2>Products</h2>
            <span></span>
          </div>
        </div>
        <div className="ibox float-e-margins">
          <div className="ibox-content">
            {!currentUser.integration('stripe') && !currentUser.integration('paypal') &&
              <div className="alert alert-danger">
                There is currently no payment source connected. If you want to sell products for money please connect a payment source in &nbsp;
                <Link to={'/user/settings/integrations'}>Settings > Integrations</Link>.
              </div>
            }
            <div className="ui cards">
              {products && products.map(product => (
                <div className="card" key={product._id}>
                  <div className="image">
                    <img src={product.image || 'http://placehold.it/800x450?text=16 / 9'} className="img-responsive center-block" />
                  </div>
                  <div className="content relative">
                    <div className="bg-danger p-10 absolute top-right m-t-15" style={{ zIndex: 1 }}>
                      {product.costFormatted({})}
                    </div>
                    <h2 className="header">{product.name}</h2>
                    {/*<p className="description">{product.description}</p>*/}
                  </div>
                  <div className="extra content">
                    <span className="right floated">
                      <span className="label label-success">{product.type}</span>
                      {product.active &&
                        <span className="label label-primary">Active</span>
                      }
                      {Memberships.hasRole('admin') && product.library &&
                        <span className="label label-primary m-l-10">Library</span>
                      }
                    </span>
                    <ul className="list-inline actions">
                      <li>
                        <Link to={router.paths.productDetails(product._id)}>
                          <i className="fa fa-edit fa-lg fa-fw" />
                        </Link>
                      </li>
                      <li>
                        <a href={product.url()} target="_blank">
                          <i className="fa fa-eye fa-lg fa-fw" />
                        </a>
                      </li>
                      <li>
                        <a href="#" onClick={() => copyProduct(product._id)}>
                          <i className="fa fa-copy fa-lg fa-fw" />
                        </a>
                      </li>
                      <li>
                        <a href="#" onClick={() => removeProduct(product._id)}>
                          <i className="fa fa-remove fa-lg fa-fw" />
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
              ))}
            </div>
          </div>
        </div>
      </div>
    )
  }
}

ProductsComponent.propTypes = {
  products: React.PropTypes.array
}

ProductsComponent.defaultProps = {

}

ProductsComponent.contextTypes = {
  router: React.PropTypes.object,
  currentUser: React.PropTypes.object
}

export default ProductsComponent
