/* global Bert */
import React from 'react'
import { Button } from 'react-bootstrap'
import { Form, Field } from 'simple-react-form'
import { TextField, SelectField, RadioField } from 'simple-react-form-bootstrap'

import { Products } from '/imports/api/products'
import { LandingPages } from '/imports/api/landingPages'

import ProductTopBanner from './ProductTopBanner'
import ProductMenu from './ProductMenu'

class ProductAdvancedComponent extends React.Component {

  constructor(props) {
    super(props)

    this.state = {
      product: props.product
    }
  }

  onSuccess() {
    Bert.alert('Product Advanced saved', 'success', 'fixed-top', 'fa-smile-o')
  }

  landingPageOptions() {
    const options = (LandingPages.find({ preDefined: { $ne: true } }).map(lp => (
      { label: lp.title, value: lp.url() }
    ))) || []
    options.unshift({ label: 'Choose Landing Page', value: '' })
    options.push({ label: 'Custom URL', value: 'custom' })
    return options
  }

  render() {
    const { product } = this.state

    return (
      <div>
        <div className="border-bottom white-bg p-t-10 p-b-10">
          <div className="container-fluid">
            <ProductTopBanner product={product} />
          </div>
        </div>
        <div className="ibox float-e-margins">
          <div className="ibox-content">
            <ProductMenu product={product} />
            <div className="m-t-20">
              <Form doc={product} collection={Products} type="update" onSuccess={() => this.onSuccess()} onChange={changes => this.setState({ product: changes })}>
                <Field fieldName="thankYouPageURL" type={SelectField} options={this.landingPageOptions()} label="Thank You Page URL" placeholder="http://" />
                <p className="m-t-n-15 text-muted"><small>This is the page your customers will be redirected to after buying your product.The default thank you page that is displayed is the "thank-you-page-template-1" to changed this edit your default settings.</small></p>
                {product.thankYouPageURL === 'custom' &&
                  <Field fieldName="thankYouPageCustomURL" type={TextField} label="Thank You Page Custom URL" placeholder="http://" />
                }
                <Field fieldName="notificationURL" type={TextField} label="Notification URL" placeholder="http://" />
                <p className="m-t-n-15 text-muted"><small>An HTTP POST will be sent to this page every time an order is made, canceled or refunded for this product.</small></p>
                <Field fieldName="termsConditionsURL" type={TextField} label="Terms & Conditions URL" placeholder="http://" />
                <p className="m-t-n-15 text-muted"><small>This creates a link to your custom Terms & Conditions page on your QuizLabs checkout page.</small></p>
                <Field fieldName="checkoutPageRedirectURL" type={TextField} label="Checkout Page Redirect URL" placeholder="http://" />
                <p className="m-t-n-15 text-muted"><small>If entered, visiting the checkout page will redirect to the URL specified. Must start with http:// or https://</small></p>
                <Button bsStyle="primary" type="submit">Save Product</Button>
              </Form>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

ProductAdvancedComponent.propTypes = {
  product: React.PropTypes.object
}

ProductAdvancedComponent.defaultProps = {

}

export default ProductAdvancedComponent
