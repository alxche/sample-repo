/* global Bert */
import { Memberships } from 'meteor/rgnevashev:memberships'

import React from 'react'
import { Nav, NavItem } from 'react-bootstrap'

import { translate } from 'react-i18next'

import { ProductPurchases } from '/imports/api/products'

class ProductLibraryComponent extends React.Component {

  constructor(props) {
    super(props)

    this.state = {
      products: props.products,
      activeKey: 'all'
    }
  }

  filter(activeKey) {
    let { products } = this.props
    const { currentUser } = this.context

    if (activeKey === 'my') {
      products = products.filter(product => product.owner === Meteor.userId())
    } else if (activeKey === 'paid') {
      products = products.filter(product => product.priceType !== 'free')
    } else if (activeKey === 'free') {
      products = products.filter(product => product.priceType === 'free')
    } else if (activeKey === 'purchased') {
      products = products.filter(product => ProductPurchases.find({ productId: product._id, userId: currentUser._id }).count())
    }

    this.setState({
      products,
      activeKey
    })
  }

  render() {
    const { t } = this.props
    const { products, activeKey } = this.state
    const { currentUser } = this.context
    const userId = currentUser._id

    return (
      <div>
        <div className="border-bottom white-bg page-heading">
          <div className="container-fluid">
            <h2>{t("title")}</h2>
          </div>
        </div>
        <div className="ibox float-e-margins">
          <div className="ibox-content">
            <Nav bsStyle="pills" className="nav-secondary space-20" activeKey={activeKey}>
              <NavItem eventKey={'all'} onClick={() => this.filter('all')}>{t("filterAll")}</NavItem>
              <NavItem eventKey={'my'} onClick={() => this.filter('my')}>{t("filterMy")}</NavItem>
              <NavItem eventKey={'paid'} onClick={() => this.filter('paid')}>{t("filterPaid")}</NavItem>
              <NavItem eventKey={'free'} onClick={() => this.filter('free')}>{t("filterFree")}</NavItem>
              <NavItem eventKey={'purchased'} onClick={() => this.filter('purchased')}>{t("filterPurchased")}</NavItem>
            </Nav>
            <div className="ui cards">
              {products && products.map(product => (
                <div className="card" key={product._id}>
                  <div className="bg-danger p-10 absolute top-right m-t-15" style={{ zIndex: 1 }}>
                    {product.costFormatted({})}
                  </div>
                  <div className="image">
                    <img src={product.image || 'http://placehold.it/800x450?text=16 / 9'} className="img-responsive center-block" />
                  </div>
                  <div className="content relative">
                    {product.hasAccess(userId) ?
                      <a href={product.accessUrl(userId)}>
                        {product.name}
                      </a> :
                      <a href={product.url()} target="_blank">
                        {product.name}
                      </a>
                    }
                  </div>
                  <div className="extra content">
                    {Memberships.hasRole('client') &&
                      <span className="right floated">
                        <span className="label label-default m-r-5">{product.type}</span>
                        {product.active ?
                          <span className="label label-primary">{t("active")}</span> :
                          <span className="label label-warning">{t("sandbox")}</span>
                        }
                      </span>
                    }
                    <span>
                      {product.hasAccess(userId) ?
                        <a href={product.accessUrl(userId)} className="btn btn-success" style={{ color: 'white' }}>
                          {t("access")}
                        </a> :
                        <a href={product.url()} className="btn btn-success" style={{ color: 'white' }} target="_blank">
                          {product.buttonText || t("buyNow")}
                        </a>
                      }
                    </span>
                  </div>
                </div>
              ))}
            </div>
          </div>
        </div>
      </div>
    )
  }
}

ProductLibraryComponent.propTypes = {
  products: React.PropTypes.array
}

ProductLibraryComponent.defaultProps = {

}

ProductLibraryComponent.contextTypes = {
  currentUser: React.PropTypes.object
}

export default translate('library', { wait: true })(ProductLibraryComponent)
