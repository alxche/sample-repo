/* global Bert */
import React from 'react'
import { Link } from 'react-router'
import { LinkContainer } from 'react-router-bootstrap'
import { Row, Col, ButtonToolbar, ButtonGroup, Button } from 'react-bootstrap'
import { Form, Field, TextField, EmailField, TextareaField, PhoneField, SelectField, ObjectField } from '/imports/lib/srf/fields'

import { refundOrder, cancelOrder, resendReceiptOrder, resendAccountSetupOrder, showAccountPassword } from '/imports/api/products/actions'

import {
  countriesOptions,
  statesOptions
} from '/imports/api/common/options'

import { ProductPurchases } from '/imports/api/products'

class OrderEditComponent extends React.Component {

  constructor(props, { router }) {
    super(props)

    this.router = router

    this.state = {
      order: props.order
    }
  }

  onSuccess() {
    Bert.alert('Order saved', 'success', 'fixed-top', 'fa-smile-o')
  }

  statusOptions() {
    return [
      { label: 'Choose Status', value: '' },
      { label: 'Succcess', value: 'succeeded' },
      { label: 'Trialing', value: 'trialing' },
      { label: 'Refunded', value: 'refunded' },
      { label: 'Deleted', value: 'deleted' },
      { label: 'Fail', value: 'fail' }
    ]
  }

  render() {
    const { order } = this.state

    return (
      <div>
        <div className="border-bottom white-bg p-t-10 p-b-10">
          <div className="container-fluid">
            <h1>Order ID #{order._id}</h1>
          </div>
        </div>
        <div className="ibox float-e-margins">
          <div className="ibox-content">
            <div className="m-t-20">
              <Form
                doc={order}
                collection={ProductPurchases}
                type="update"
                onSuccess={() => this.onSuccess()}
                onChange={changes => this.setState({ order: changes })}
              >
                <Row>
                  <Col md={4}>
                    <h3>Order Date</h3>
                    <p>{order.purchasedAt()}</p>
                  </Col>
                  <Col md={4}>
                    <Field fieldName="status" type={SelectField} options={this.statusOptions()} />
                  </Col>
                  <Col md={4}>
                    <h3>Affiliate</h3>
                    <p>N/A</p>
                  </Col>
                </Row>
                <hr />
                <Row>
                  <Col md={4}>
                    <div className="ui segment">
                      <h3>Customer Contact</h3>
                      <Field fieldName="firstName" type={TextField} placeholder="First Name" />
                      <Field fieldName="lastName" type={TextField} placeholder="last Name" />
                      <Field fieldName="email" type={EmailField} placeholder="Email" />
                      <Field fieldName="phone" type={PhoneField} placeholder="Phone" />
                    </div>
                  </Col>
                  <Col md={4}>
                    <div className="ui segment">
                      <Field fieldName="billingAddress" type={ObjectField} label="Billing Address">
                        <Field fieldName="line1" type={TextField} placeholder="Address 1" />
                        <Field fieldName="line2" type={TextField} placeholder="Address 2" />
                        <div className="row">
                          <div className="col-sm-4">
                            <Field fieldName="zip" type={TextField} placeholder="Zip" />
                          </div>
                          <div className="col-sm-4">
                            <Field fieldName="city" type={TextField} placeholder="City" />
                          </div>
                          <div className="col-sm-4">
                            {(order.billingAddress && order.billingAddress.country === 'us') ?
                              <Field fieldName="state" type={SelectField} options={statesOptions} placeholder="State" /> :
                              <Field fieldName="county" type={TextField} placeholder="County" />
                            }
                          </div>
                        </div>
                        <Field fieldName="country" type={SelectField} options={countriesOptions} placeholder="Country" />
                      </Field>
                    </div>
                  </Col>
                  <Col md={4}>
                    <div className="ui segment">
                      <Field fieldName="shippingAddress" type={ObjectField} label="Shipping Address">
                        <Field fieldName="line1" type={TextField} placeholder="Address 1" />
                        <Field fieldName="line2" type={TextField} placeholder="Address 2" />
                        <div className="row">
                          <div className="col-sm-4">
                            <Field fieldName="zip" type={TextField} placeholder="Zip" />
                          </div>
                          <div className="col-sm-4">
                            <Field fieldName="city" type={TextField} placeholder="City" />
                          </div>
                          <div className="col-sm-4">
                            {(order.shippingAddress && order.shippingAddress.country === 'us') ?
                              <Field fieldName="state" type={SelectField} options={statesOptions} placeholder="State" /> :
                              <Field fieldName="county" type={TextField} placeholder="County" />
                            }
                          </div>
                        </div>
                        <Field fieldName="country" type={SelectField} options={countriesOptions} placeholder="Country" />
                      </Field>
                    </div>
                  </Col>
                </Row>
                <hr />
                <h2>Order Summary</h2>
                <table className="table">
                  <thead>
                    <tr>
                      <td width="20%"><strong>Item</strong></td>
                      <td width="10%"><strong>Price</strong></td>
                      <td width="10%"><strong>Qty</strong></td>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>
                        <Link to={this.router.paths.productDetails(order.productId)}>
                          {order.product().name}
                        </Link>
                      </td>
                      <td>
                        {order.costFormatted()}
                      </td>
                      <td>{order.quantity || 1}</td>
                    </tr>
                    {order.subtotal ?
                      <tr>
                        <td>&nbsp;</td>
                        <td>Sub Total</td>
                        <td>{order.subtotalFormatted()}</td>
                      </tr> : null
                    }
                    {order.shippingCost ?
                      <tr>
                        <td>&nbsp;</td>
                        <td>Shipping Costs</td>
                        <td>{order.shippingCostFormatted()}</td>
                      </tr> : null
                    }
                    {order.trialFormatted ?
                      <tr>
                        <td>&nbsp;</td>
                        <td>Trial</td>
                        <td>{order.trialFormatted()}</td>
                      </tr> : null
                    }
                    {order.discount ?
                      <tr>
                        <td>&nbsp;</td>
                        <td>Discount</td>
                        <td>{order.discountFormatted()}</td>
                      </tr> : null
                    }
                    <tr>
                      <td>&nbsp;</td>
                      <td>Total</td>
                      <td>{order.totalFormatted()}</td>
                    </tr>
                  </tbody>
                </table>
                <Field fieldName="notes" type={TextareaField} placeholder="Notes" />

                <ButtonToolbar>
                  <Button bsStyle="primary" type="submit">Save</Button>
                  <Button bsStyle="primary" onClick={() => resendReceiptOrder(order._id)}>Resend Receipt</Button>
                  <Button bsStyle="primary" onClick={() => showAccountPassword(order.userId)}>Show Password</Button>
                  <Button bsStyle="primary" onClick={() => resendAccountSetupOrder(order.userId)}>Resend Account Access</Button>
                  <LinkContainer to={this.router.paths.customerOrders(order.userId)}>
                    <Button bsStyle="primary">View Purchases</Button>
                  </LinkContainer>
                  <ButtonGroup>
                    {(order.priceType !== 'free' && order.status !== 'refunded') &&
                      <Button
                        bsStyle="danger"
                        onClick={() => refundOrder(order._id)}
                      >
                        Refund Entire Order
                      </Button>
                    }
                    <Button
                      bsStyle="danger"
                      onClick={() =>
                        cancelOrder(order._id, (err) => {
                          if (!err) {
                            this.router.push('/orders')
                          }
                        })
                      }
                    >
                      Cancel & Revoke Access
                    </Button>
                  </ButtonGroup>
                  <LinkContainer to={this.router.paths.orderView(order._id)}>
                    <Button bsStyle="default">Back to View</Button>
                  </LinkContainer>
                </ButtonToolbar>
              </Form>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

OrderEditComponent.propTypes = {
  order: React.PropTypes.object
}

OrderEditComponent.defaultProps = {

}

OrderEditComponent.contextTypes = {
  router: React.PropTypes.object
}

export default OrderEditComponent
