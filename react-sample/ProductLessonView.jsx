import React from 'react'
import moment from 'moment'

import ContentSectionView from '../common/ContentSectionView.jsx'

class ProductLessonView extends React.Component {

  constructor(props) {
    super(props)

    this.state = {
    }
  }

  render() {
    const { lesson, order } = this.props

    if (!lesson) {
      return <div />
    }

    return (
      <div className="white-bg product-lesson-container p-15">
        {lesson.hasAccess() ?
          <div>
            <h1 className="m-none text-center text-black">{lesson.title}</h1>
            {lesson.sections && lesson.sections.map((section, index) => (
              <ContentSectionView
                key={`product-lesson-section-${index}`}
                className="product-lesson-content"
                section={{ ...section, subSection: true }}
              />
            ))}
          </div> :
          <div>
            {lesson.reasonDeny() === 'plan' &&
              <div className="text-center">
                <h2>Your subscription does not give you access to this lecture</h2>
                <button className="btn btn-primary btn-lg">Upgrade My Subscription Now</button>
              </div>
            }
            {lesson.reasonDeny() === 'package' &&
              <div className="text-center">
                <h2>Your package does not give you access to this lecture</h2>
                <button className="btn btn-primary btn-lg">Upgrade My Package Now</button>
              </div>
            }
            {order && lesson.reasonDeny() === 'dripReleaseDays' &&
              <div className="text-center">
                <h2>
                  Your Drip Release Date is <strong>{moment(order.createdAt).add(lesson.daysFromPurchase, 'days').format('MM/DD/YYYY')}</strong>
                </h2>
              </div>
            }
            {lesson.reasonDeny() === 'dripReleaseAt' &&
              <div className="text-center">
                <h2>
                  Your Drip Release Date is <strong>{moment(lesson.dripReleaseAt).format('MM/DD/YYYY')}</strong>
                </h2>
              </div>
            }
          </div>
        }
      </div>
    )
  }
}

ProductLessonView.propTypes = {
  lesson: React.PropTypes.object,
  order: React.PropTypes.object
}

ProductLessonView.contextTypes = {
  currentUser: React.PropTypes.object
}

export default ProductLessonView
