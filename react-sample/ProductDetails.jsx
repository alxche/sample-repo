/* global Bert */
import { Memberships } from 'meteor/rgnevashev:memberships'

import React from 'react'
import { Button } from 'react-bootstrap'
import { Form, Field } from 'simple-react-form'
import { TextField, TextareaField, SelectField, RadioField, ArrayField, CheckboxesField } from 'simple-react-form-bootstrap'
import DropzoneField from '/imports/lib/fields/Dropzone'
import ToggleField from '/imports/lib/fields/Toggle'

import { Products } from '/imports/api/products'

import ProductTopBanner from './ProductTopBanner'
import ProductMenu from './ProductMenu'

class ProductDetailsComponent extends React.Component {

  constructor(props) {
    super(props)

    this.state = {
      product: props.product
    }
  }

  onSuccess() {
    Bert.alert('Product Details saved', 'success', 'fixed-top', 'fa-smile-o')
  }

  renderPlan(item) {
    _.defaults(item, {
      currency: 'usd'
    })
    return (
      <div className="ui segment m-b-20">
        <div className="row">
          <div className="col-md-4">
            <Field fieldName="name" type={TextField} placeholder="Plan Name" showLabel={false} />
          </div>
          <div className="col-md-3">
            <Field fieldName="amount" type={TextField} placeholder="Amount" showLabel={false} />
          </div>
          <div className="col-md-3">
            <Field fieldName="currency" type={SelectField} showLabel={false} />
          </div>
          <div className="col-sm-2">
            <Field fieldName="asDefault" type={ToggleField} />
          </div>
        </div>
        <div className="row">
          <div className="col-md-3">
            <Field fieldName="interval_count" type={TextField} placeholder="1" showLabel={false} />
          </div>
          <div className="col-md-3">
            <Field fieldName="interval" type={SelectField} showLabel={false} />
          </div>
          <div className="col-md-3">
            <Field fieldName="trial_period_amount" type={TextField} placeholder="Trial Amount" showLabel={false} />
          </div>
          <div className="col-md-3">
            <Field fieldName="trial_period_days" type={TextField} placeholder="Trial in Days" showLabel={false} />
          </div>
        </div>
      </div>
    )
  }

  renderLevel(item) {
    _.defaults(item, {
      currency: 'usd'
    })
    return (
      <div className="ui segment m-b-20">
        <div className="row">
          <div className="col-sm-6">
            <Field fieldName="name" type={TextField} />
          </div>
          <div className="col-sm-2">
            <Field fieldName="price" type={TextField} />
          </div>
          <div className="col-sm-2">
            <Field fieldName="currency" type={SelectField} />
          </div>
          <div className="col-sm-2">
            <Field fieldName="asDefault" type={ToggleField} />
          </div>
        </div>
      </div>
    )
  }

  render() {
    const { currentUser } = this.context
    const { product } = this.state
    let options = []
    _.each(currentUser.integrations,(data,integration)=>{
      const service = _.findWhere(this.props.integrationVendors,{_id:integration,paymentGateway:true})
      if(service){
        options.push({label:service.name,value:service._id})
      }
    })

    return (
      <div>
        <div className="border-bottom white-bg p-t-10 p-b-10">
          <div className="container-fluid">
            <ProductTopBanner product={product} />
          </div>
        </div>
        <div className="ibox float-e-margins">
          <div className="ibox-content">
            <ProductMenu product={product} />
            <div className="m-t-20">
              <Form doc={product} collection={Products} type="update" onSuccess={() => this.onSuccess()} onChange={changes => this.setState({ product: changes })}>
                <Field fieldName="name" type={TextField} />
                <Field fieldName="description" type={TextareaField} />
                <Field fieldName="image" type={DropzoneField} crop />
                {Memberships.hasRole('admin') &&
                  <div>
                    <Field fieldName="library" type={ToggleField} label="Show in Library" />
                  </div>
                }
                <Field fieldName="priceType" type={RadioField} inline />
                {product.priceType === 'fixed' &&
                  <div className="row">
                    <div className="col-sm-6">
                      <Field fieldName="price" type={TextField} />
                    </div>
                    <div className="col-sm-6">
                      <Field fieldName="currency" type={SelectField} />
                    </div>
                    <div className="col-sm-6">
                      <Field fieldName="paymentProviders" type={CheckboxesField} options={options}/>
                    </div>
                  </div>
                }
                {product.priceType === 'recurring' &&
                  <div>
                    <p>Allows you to create recurring products.</p>
                    <Field fieldName="plans" type={ArrayField} renderItem={this.renderPlan} />
                  </div>
                }
                {product.priceType === 'level' &&
                  <div>
                    <p>Allows you to create different variations of each product. You might have a lite version and a pro version of the product for example. This allows you to manage the content within one lessons area for all your packages. You attach the access permissions for each lesson.</p>
                    <Field fieldName="packages" type={ArrayField} renderItem={this.renderLevel} />
                    <div className="col-sm-6">
                      <Field fieldName="paymentProviders" type={CheckboxesField} options={options}/>
                    </div>
                  </div>
                }
                <Field fieldName="rewardId" type={SelectField} options={product.rewardOptions()} label="Reward" />
                <Button bsStyle="primary" type="submit">Save Product</Button>
              </Form>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
ProductDetailsComponent.contextTypes = {
  currentUser: React.PropTypes.object
}
ProductDetailsComponent.propTypes = {
  product: React.PropTypes.object,
  integrationVendors: React.PropTypes.array
}

ProductDetailsComponent.defaultProps = {

}

export default ProductDetailsComponent
