import React from 'react'
import { Link } from 'react-router'
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table'

import { Rewards } from '/imports/api/products'

import { confirm } from '/imports/lib/actions'

class RewardTableComponent extends React.Component {

  constructor(props, { router }) {
    super(props)

    this.actionsCell = this.actionsCell.bind(this)

    this.router = router

    this.state = {
    }
  }

  activeReward(rewardId) {
    Rewards.update(rewardId, {
      $set: {
        active: true
      }
    })
  }

  offReward(rewardId) {
    confirm({

    }, () => Rewards.update(rewardId, {
      $set: {
        active: false
      }
    }))
  }

  removeReward(rewardId) {
    confirm({}, () => Rewards.remove(rewardId))
  }

  actionsCell(rewardId, reward) {
    return (
      <div>
        {reward.active ?
          <button className="btn btn-default" type="button" onClick={() => this.offReward(reward._id)}>Active</button> :
          <button className="btn btn-default" type="button" onClick={() => this.activeReward(reward._id)}>Off</button>
        }
        <Link to={this.router.paths.rewardEdit(rewardId)} className="btn btn-default" title="Edit">
          <i className="fa fa-edit" />
        </Link>
        <a href="#" className="btn btn-default" onClick={() => this.removeReward(rewardId)} title="Remove">
          <i className="fa fa-remove" />
        </a>
      </div>
    )
  }

  render() {
    const { rewards } = this.props
    const options = { defaultSortName: 'createdAt', defaultSortOrder: 'desc', sizePerPage: 25 }

    return (
      <BootstrapTable data={rewards} options={options} striped hover pagination search>
        <TableHeaderColumn dataField="name" dataSort>Name</TableHeaderColumn>
        <TableHeaderColumn dataField="type" dataSort>Type</TableHeaderColumn>
        <TableHeaderColumn dataField="createdAt" hidden />
        <TableHeaderColumn dataField="_id" dataFormat={this.actionsCell} export={false} isKey>Actions</TableHeaderColumn>
      </BootstrapTable>
    )
  }
}

RewardTableComponent.propTypes = {
  rewards: React.PropTypes.array
}

RewardTableComponent.defaultProps = {

}

RewardTableComponent.contextTypes = {
  router: React.PropTypes.object
}

export default RewardTableComponent
