taxiCtrls

  // All this does is allow the message
  // to be sent when you tap return
  .directive('input', function($timeout) {
    return {
      restrict: 'E',
      scope: {
        'returnClose': '=',
        'onReturn': '&',
        'onFocus': '&',
        'onBlur': '&'
      },
      link: function(scope, element, attr) {
        element.bind('focus', function(e) {
          if (scope.onFocus) {
            $timeout(function() {
              scope.onFocus();
            });
          }
        });
        element.bind('blur', function(e) {
          if (scope.onBlur) {
            $timeout(function() {
              scope.onBlur();
            });
          }
        });
        element.bind('keydown', function(e) {
          if (e.which == 13) {
            if (scope.returnClose) element[0].blur();
            if (scope.onReturn) {
              $timeout(function() {
                scope.onReturn();
              });
            }
          }
        });
      }
    }
  })


  .controller('ChatCtrl', function(Semaphore, $scope, $localStorage, $timeout, $ionicScrollDelegate, $http, CONFIG, $translate) {

    var alternate,
      isIOS = ionic.Platform.isWebView() && ionic.Platform.isIOS();

    $scope.$translate = $translate;
    $scope.sendMessage = function() {

      if(!angular.isDefined($scope.data.message) || $scope.data.message=='') return;

      //Semaphore: Ставим ограничение, текст ошибки НЕ возвращается, сообщаем о проблеме инета
      if(!Semaphore.canGo('pushMessage', Semaphore.USER_ACTION)) return;

      $http.post(CONFIG.API_WWW_PATH+'order/pushMessage', { order_id: $localStorage.order.order_id, is_driver:0, message: $scope.data.message}, CONFIG.HTTP_CONFIG)
        .success(function() {
            Semaphore.Release('pushMessage');
        })
        .error(function(data) {
            Semaphore.Release('pushMessage');
            Semaphore.ITERNET_ERROR($translate('CHAT_CANTSEND'));
        });

      delete $scope.data.message;
      $ionicScrollDelegate.scrollBottom(true);

    };


    $scope.inputUp = function() {
      if (isIOS) $scope.data.keyboardHeight = 216;
      $timeout(function() {
        $ionicScrollDelegate.scrollBottom(true);
      }, 300);

    };

    $scope.inputDown = function() {
      if (isIOS) $scope.data.keyboardHeight = 0;
      $ionicScrollDelegate.resize();
    };

    $scope.closeKeyboard = function() {
      cordova.plugins.Keyboard.close();
    };


    $scope.data = {};
    $scope.myId = 'Driver';

    $scope.$watch(function() {
      if($localStorage.order && $localStorage.order.messages)
      return angular.toJson($localStorage.order.messages);
    }, function() {
      if($localStorage.order && $localStorage.order.messages) {
        $localStorage.order.chatSeen = _.max($localStorage.order.messages, function (msg) {
          return msg.message_id;
        }).message_id;
        $timeout(function () {
          $ionicScrollDelegate.scrollBottom(true);
        }, 300);
      }
    });

    $scope.$storage = $localStorage;

  });
