taxiCtrls
.service('geoPointsService', function () {
  return {
    geoPoint: null,
    pickupPoint: null,
    destinationPoint: null
  };
})
.controller('GeoSearchCtrl', function(geoPointsService, HistoryLib, $scope, $timeout, $localStorage, $rootScope, $state) {

  this.main={show_list: 'search'};

  this.addBookmark = function(place){
    console.log(place,HistoryLib.addBookmarkPlaces(place));
  };

  $scope.placesBookmarks=HistoryLib.getBookmarkPlaces();
  $scope.placesHistory=HistoryLib.getLastPlaces();

  $scope.places=[];
  var map = $rootScope.map;
  map.setZoom(12);
  var bounds = map.getBounds();
  map.setZoom(17);
  var getPlaces = function(q){
    $scope.places=[];
    var request = {
      input: q,
      bounds: bounds,
      types: ['address']
    };

    var placesService = new google.maps.places.PlacesService(map);

    var acService = new google.maps.places.AutocompleteService(map);

    acService.getQueryPredictions(request, function(results){
      if(results) {
        for (var i = 0; i < results.length; i++) {
          var place = results[i];

          if (place.place_id) {
            placesService.getDetails({placeId: place.place_id}, function (res, status) {
              if(res && res.address_components) {
                for (var i = 0; i < res.address_components.length; i++) {
                  var addr = res.address_components[i];
                  var country;
                  if (addr.types[0] == 'country')
                    country = addr.long_name;
                }
                console.log(country);
                if (res && country == "Cyprus") {
                  $scope.places.push(res);
                  $scope.$apply();
                }
              }
            });

          }
        }
      }
    });
  };
    this.goToPlace = function(place){
        console.log(typeof(place.geometry.location.lat));

      if(!geoPointsService.pickupPoint)
        geoPointsService.pickupPoint=place;
      else
        geoPointsService.destinationPoint=place;
      var latlng = new google.maps.LatLng(place.geometry.location.lat, place.geometry.location.lng);
      $localStorage.setCenter={lat:place.geometry.location.lat, lng:place.geometry.location.lng};

      HistoryLib.addLastPlaces(place);

      $state.go("app.map");
    };


  var tempFilterText='',
    filterTextTimeout;
  $scope.$watch("searchString", function (val) {
    if($scope.searchString && $scope.searchString.length>2){

      if (filterTextTimeout) $timeout.cancel(filterTextTimeout);

      tempFilterText = val;
      filterTextTimeout = $timeout(function() {
        getPlaces(val);
      }, 250); // delay 250 ms
    }
  }, true);


});
