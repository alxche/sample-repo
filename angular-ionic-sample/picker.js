!function() {
  "use strict";

  angular.module("dt-picker", ["ionic", 'ionic-toast'])
    .directive("dtPicker", function () {
      return {
        restrict: "AE",
        require: "ngModel",
        scope: {
          modelDate: "=ngModel",
          title: "=",
          subTitle: "=",
          buttonOk: "=",
          buttonCancel: "="
        },
        controller: function ($scope, $ionicPopup, $ionicPickerI18n, $timeout,ionicToast,$translate,$localStorage) {

          if($localStorage.translation.currentLang=="ru") {
            $ionicPickerI18n.weekdays = ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"];
            $ionicPickerI18n.months = ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"];
          }
          if($localStorage.translation.currentLang=="gr") {
            $ionicPickerI18n.weekdays = ["Κυρ","Δευ","Τρι","Τετ","Πεμ","Παρ","Σαβ" ];
            $ionicPickerI18n.months = ["Ιανουάριος", "Φεβρουάριος", "Μάρτιος", "Απρίλιος", "Μάιος", "Ιούνιος", "Ιούλιος", "Αύγουστος", "Σεπτέμβριος", "Οκτώβριος", "Νοέμβριος", "Δεκέμβριος"];
          }
          $ionicPickerI18n.ok = $translate("COMMON_OK");
          $ionicPickerI18n.cancel = $translate("COMMON_CANCEL");

          $scope.checkValue = function (date) {
            if(date){
              if($scope.minDate) {
                var pDate = $scope.$parent.$parent.transfer.pickupDate;
                if(moment(date).isBefore(momentServer(pDate).add($scope.minDate,"days"))){
                  ionicToast.show($translate("CALEND_ROUNDMIN",{days: $scope.minDate}), 'middle', false, 3000);
                  return false;
                }
              }
              if($scope.maxDays){
                if(moment(date).isAfter(momentServer().add($scope.maxDays,"days"))){
                  ionicToast.show($translate("CALEND_MAXDAYS",{days: $scope.maxDays}), 'middle', false, 3000);
                  return false;
                }
              }
              if($scope.fromNow || true){
                if(!moment(date).isAfter(momentServer().subtract(60,"seconds"))){
                  ionicToast.show($translate("CALEND_PASTDATE"), 'middle', false, 3000);
                  return false;
                }
              }
              if($scope.transferMin){
                if(!moment(date).isAfter(momentServer().add($scope.transferMin,"minutes"))){
                  ionicToast.show($translate("CALEND_MIN",{min: $scope.transferMin}), 'middle', false, 3000);
                  return false;
                }
              }
              return true;
            }
          };
          $scope.timeNow = function () {
            return momentServer().locale('ru');
          };

          $scope.i18n = $ionicPickerI18n;
          $scope.bind = {};

          $scope.rows = [0, 1, 2, 3, 4, 5];
          $scope.cols = [1, 2, 3, 4, 5, 6, 7];
          $scope.weekdays = [0, 1, 2, 3, 4, 5, 6];

          $scope.showPopup = function () {
            $scope.thisPopup=$ionicPopup.show({
              templateUrl: "templates/picker-popup.html",
              title: $translate("CALEND_TITLE") || ("Pick " + ($scope.dateEnabled ? "a date" : "") + ($scope.dateEnabled && $scope.timeEnabled ? " and " : "") + ($scope.timeEnabled ? "a time" : "")),
              subTitle: $scope.subTitle || "",
              scope: $scope,
              cssClass: 'ion-datetime-picker-popup',
              buttons: [
                {
                  text: '<i class="icon ion-close-circled"></i>',
                  type:'popclose',
                  onTap: function(e) {
                    $scope.thisPopup.close()
                  }
                },
                {
                  text: $translate("CALEND_NOW"),
                  type: "button-assertive",
                  onTap: function (e) {
                    var date = momentServer().toDate();
                    $scope.year = date.getFullYear();
                    $scope.month = date.getMonth();
                    $scope.day = date.getDate();
                    $scope.hour = date.getHours();
                    $scope.minute = date.getMinutes();
                    changeViewData();
                    $scope.commit();
                    $timeout(function(){$scope.$parent.$parent.$storage.order.pickupDate = null;},100);
                  }
                },
                {
                  text: $translate("CALEND_30MIN"),
                  type: "button-calm",
                  onTap: function (e) {
                    $timeout(function () {
                      var date = momentServer().add(30, "minutes").toDate();
                      $scope.year = date.getFullYear();
                      $scope.month = date.getMonth();
                      $scope.day = date.getDate();
                      $scope.hour = date.getHours();
                      $scope.minute = date.getMinutes();
                      changeViewData();
                      $scope.commit();
                    }, 100);
                  }
                },
                {
                  text: $translate("CALEND_TODAY"),
                  type: "button-calm",
                  onTap: function (e) {
                    $timeout(function () {
                      var date = momentServer().add(30, "minutes").toDate();
                      $scope.year = date.getFullYear();
                      $scope.month = date.getMonth();
                      $scope.day = date.getDate();
                      changeViewData();
                    }, 100);
                    e.preventDefault();
                  }
                },
                {
                  text: $translate("CALEND_SET"),
                  type: "button-positive",
                  onTap: function (e) {
                    e.preventDefault();
                    $scope.commit();
                  }
                }
              ]
            });
          };

          $scope.prepare = function () {
            if ($scope.mondayFirst) {
              $scope.weekdays.push($scope.weekdays.shift());
            }
          };

          $scope.processModel = function () {
            $timeout(function () {
              var date = $scope.modelDate instanceof Date ? $scope.modelDate : momentServer().toDate();
              $scope.year = $scope.dateEnabled ? date.getFullYear() : 0;
              $scope.month = $scope.dateEnabled ? date.getMonth() : 0;
              $scope.day = $scope.dateEnabled ? date.getDate() : 0;
              $scope.hour = $scope.timeEnabled ? date.getHours() : 0;
              $scope.minute = $scope.timeEnabled ? date.getMinutes() : 0;
              $scope.second = $scope.secondsEnabled ? date.getSeconds() : 0;

              changeViewData();
            }, 500);
          };

          var changeViewData = function () {
            var date = new Date($scope.year, $scope.month, $scope.day, $scope.hour, $scope.minute, $scope.second);

            if ($scope.dateEnabled) {
              $scope.year = date.getFullYear();
              $scope.month = date.getMonth();
              $scope.day = date.getDate();

              $scope.bind.year = $scope.year;
              $scope.bind.month = $scope.month.toString();

              $scope.firstDay = new Date($scope.year, $scope.month, 1).getDay();
              if ($scope.mondayFirst) {
                $scope.firstDay = ($scope.firstDay || 7) - 1;
              }
              $scope.daysInMonth = new Date($scope.year, $scope.month + 1, 0).getDate();
              if ($scope.day > $scope.daysInMonth) {
                $scope.day = $scope.daysInMonth;
              }
            }

            if ($scope.timeEnabled) {
              $scope.hour = date.getHours();
              $scope.minute = date.getMinutes();
              $scope.second = date.getSeconds();
              $scope.meridiem = $scope.hour < 12 ? "AM" : "PM";

              $scope.bind.hour = $scope.meridiemEnabled ? ($scope.hour % 12 || 12).toString() : $scope.hour.toString();
              $scope.bind.minute = ($scope.minute < 10 ? "0" : "") + $scope.minute.toString();
              $scope.bind.second = ($scope.second < 10 ? "0" : "") + $scope.second.toString();
              $scope.bind.meridiem = $scope.meridiem;
            }
          };

          $scope.changeBy = function (value, unit) {
            if (+value) {
              // DST workaround
              if ((unit === "hour" || unit === "minute") && value === -1) {
                var date = new Date($scope.year, $scope.month, $scope.day, $scope.hour - 1, $scope.minute);
                if (($scope.minute === 0 || unit === "hour") && $scope.hour === date.getHours()) {
                  $scope.hour--;
                }
              }
              $scope[unit] += +value;
              changeViewData();
            }
          };
          $scope.change = function (unit) {
            var value = $scope.bind[unit];
            if (value && unit === "meridiem") {
              value = value.toUpperCase();
              if (value === "AM" && $scope.meridiem === "PM") {
                $scope.hour -= 12;
              } else if (value === "PM" && $scope.meridiem === "AM") {
                $scope.hour += 12;
              }
              changeViewData();
            } else if (+value || value === "0") {
              $scope[unit] = +value;
              changeViewData();
            }
          };
          $scope.changeDay = function (day) {
            $scope.day = day;
            changeViewData();
          };
          $scope.changed = function () {
            changeViewData();
          };
          $timeout(function () {

            if ($scope.dateEnabled) {
              $scope.$watch(function () {
                return momentServer().toDate().getDate();
              }, function () {
                var today = momentServer().toDate();//new Date();
                $scope.today = {
                  day: today.getDate(),
                  month: today.getMonth(),
                  year: today.getFullYear()
                };

              });
//                    $scope.goToToday = function() {
//                        $scope.year = $scope.today.year;
//                        $scope.month = $scope.today.month;
//                        $scope.day = $scope.today.day;
//
//                        changeViewData();
//                    };
            }
          }, 100);
        },
        link: function ($scope, $element, $attrs, ngModelCtrl,ionicToast) {

          $scope.dateEnabled = "date" in $attrs && $attrs.date !== "false";
          $scope.timeEnabled = "time" in $attrs && $attrs.time !== "false";
          if ($scope.dateEnabled === false && $scope.timeEnabled === false) {
            $scope.dateEnabled = $scope.timeEnabled = true;
          }

          $scope.mondayFirst = "mondayFirst" in $attrs && $attrs.mondayFirst !== "false";
          $scope.fromNow = "fromNow" in $attrs && $attrs.fromNow !== "false";
          $scope.transferMin = ("transferMin" in $attrs && $attrs.transferMin !== "false")?$attrs.transferMin:false;
          $scope.maxDays = ("maxDays" in $attrs && $attrs.maxDays !== "false")?$attrs.maxDays:false;
          $scope.minDate = ("minDate" in $attrs)?$attrs.minDate:false;
          $scope.secondsEnabled = $scope.timeEnabled && "seconds" in $attrs && $attrs.seconds !== "false";
          $scope.meridiemEnabled = $scope.timeEnabled && "amPm" in $attrs && $attrs.amPm !== "false";


          $scope.prepare();

          ngModelCtrl.$render = function () {
            $scope.modelDate = ngModelCtrl.$viewValue;
            $scope.processModel();
          };


          $scope.commit = function () {
            console.log("checker:", $scope.checkValue($scope.modelDate));
            if ($scope.checkValue(new Date($scope.year, $scope.month, $scope.day, $scope.hour, $scope.minute, $scope.second)))
            {
              $scope.modelDate = new Date($scope.year, $scope.month, $scope.day, $scope.hour, $scope.minute, $scope.second);
              ngModelCtrl.$setViewValue($scope.modelDate);
              $scope.thisPopup.close()
            }
          };

          $element.on("click", $scope.showPopup);
        }
      };
    });
}();
