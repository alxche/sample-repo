taxiCtrls
    .controller('ProfileCtrl', function(Semaphore, $scope, $cordovaCamera, $state, $http, ionicToast, CONFIG, $translate, $localStorage) {

        $scope.userData = angular.copy($localStorage.userData);
        $scope.newPassword = {passwordNew:null,passwordOld:null}

        $scope.resend = function(){
            $http.post(CONFIG.API_WWW_PATH+'util/resendCode', {client_id: $scope.userData.client_id}, CONFIG.HTTP_CONFIG)
                .success(function (data) {
                    swal($translate('COMMON_OK'),'', "info");
                })
                .error(function (data) {
                });
        }
        $scope.changePassword = function(){

            $http.post(CONFIG.API_WWW_PATH+'util/changePassword', _.extend($scope.newPassword,{client_id: $scope.userData.client_id}), CONFIG.HTTP_CONFIG)
                .success(function (data) {
                    $scope.newPassword = {passwordNew:null,passwordOld:null}
                    swal($translate('COMMON_OK'),'', "info");
                })
                .error(function (data) {
                    swal($translate('PROFILE_PASSWORDERROR'),'', "info");
                });
        }
        $scope.confirmPhone = function(){
            swal({
                    title: $translate("PROFILE_ENTERCODE"),
                    type: "input",
                    showCancelButton: true,
                    confirmButtonColor: "#EF473A",
                    confirmButtonText: $translate("COMMON_OK"),
                    cancelButtonText: $translate("COMMON_CANCEL"),
                    cancelButtonColor: "#33CD5F",
                    closeOnConfirm: false,
                    closeOnCancel: true,
                },
                function(inputValue){
                    if (inputValue === false) return false;
                    console.log(inputValue,inputValue.trim(),inputValue.length)
                    $http.post(CONFIG.API_WWW_PATH+'util/confirmPhone', {code: inputValue.trim()+"", client_id: $scope.userData.client_id}, CONFIG.HTTP_CONFIG)
                        .success(function (data) {
                            swal($translate('COMMON_OK'),'', "info");
                            $scope.$storage.userData = data;
                            $scope.userData = data;
                        })
                        .error(function (data) {
                            swal($translate('PROFILE_BADCODE'), $translate('PROFILE_TRYCODE'), "warning");
                        });
                });
        }

        $scope.takePhoto = function(){

            var options = {
                quality: 75,
                destinationType: Camera.DestinationType.DATA_URL,
                sourceType: Camera.PictureSourceType.CAMERA,
                allowEdit: true,
                encodingType: Camera.EncodingType.PNG,
                targetWidth: CONFIG.AVATAR_D,
                targetHeight: CONFIG.AVATAR_D,
                popoverOptions: CameraPopoverOptions,
                saveToPhotoAlbum: false
            };

            $cordovaCamera.getPicture(options).then(function (imageData) {

                var imgData = "data:image/png;base64," + imageData;
                $scope.userData.imgURI = imgData;
                $scope.userData.action = 'add_photo';
                $scope.userData.photo = imgData;

            }, function (err) {
                console.log('takePhoto > getPicture error');
            });
        }

        $scope.choosePhoto = function () {
            var options = {
                quality: 75,
                destinationType: Camera.DestinationType.DATA_URL,
                sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
                allowEdit: true,
                encodingType: Camera.EncodingType.PNG,
                targetWidth: CONFIG.AVATAR_D,
                targetHeight: CONFIG.AVATAR_D,
                popoverOptions: CameraPopoverOptions,
                saveToPhotoAlbum: false
            };
            $cordovaCamera.getPicture(options).then(function (imageData) {

                var imgData = "data:image/png;base64," + imageData;
                $scope.userData.imgURI = imgData;
                $scope.userData.action = 'add_photo';
                $scope.userData.photo = imgData;

            }, function (err) {
                console.log('choosePhoto > getPicture error');
            });
        }

        $scope.clearPhoto = function () {
            $scope.userData.imgURI = null;
            $scope.userData.action = 'delete_photo';
            $scope.userData.photo = 'img/default-avatar.png';
        }


        $scope.saveProfile = function () {

            //Semaphore: Ставим ограничение, текст ошибки НЕ возвращается, сообщаем о проблеме инета
            if(!Semaphore.canGo('saveProfile', Semaphore.USER_ACTION)) return;

            $http.post(CONFIG.API_WWW_PATH+'util/saveProfile', $scope.userData, CONFIG.HTTP_CONFIG)
                .success(function(updatedData) {
                    Semaphore.Release('saveProfile');
                    $scope.$storage.userData = updatedData;
                    $scope.userData = updatedData;
                    ionicToast.show($translate("PROFILE_SAVED"), 'middle', false, 1500);
                    setTimeout(function() {$state.go("app.back");}, 1700);
                })
                .error(function(data) {
                    Semaphore.Release('saveProfile');
                    Semaphore.QUERY_ERROR(data, $translate("PROFILE_FAILED"));
                });
        }
    });


